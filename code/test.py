import os
import matplotlib.pyplot as plt
plt.interactive(False)
import bequest
import bequestOld
plt.ioff()
import numpy as np

# the following: Identital
figuresOld = bequestOld.questions1and2()

figuresNew = bequest.questions1and2()

i = 1
for fig in figuresOld:
    if fig != 'fig0':
        plt.close(figuresOld[fig])
        plt.close(figuresNew[fig])
    i+=1


ParamEndog = bequest.Parameters()
ParamEndog2 = bequest.Parameters()
np.random.seed(1)
thetaNoDeath = np.hstack((np.ones((ParamEndog.T-1)), 0))
policies = bequest.solve(bequest.solveEndogenousGridPoints, thetaNoDeath, bequest.WBequestPrime, ParamEndog)
np.random.seed(1)
policies2 = bequestOld.solve(bequestOld.solveEndogenousGridPoints, thetaNoDeath, bequestOld.WBequestPrime, ParamEndog2)

(policies[0] - policies2[0]).max()



policies = bequest.solve(bequest.solveEndogenousGridPoints, thetaNoDeath, bequest.WBequestPrime, ParamEndog,
                         r = ParamEndog.r, w=10)
S, X, Y = bequest.simulate(policies, ParamEndog)
fig0 = plt.figure()
plt.plot(S.mean(axis=0), label='median S')
plt.plot((X - S).mean(axis=0), label='median C')
plt.plot(Y.mean(axis=0), label='median Y')
plt.title('No Death')
plt.legend()

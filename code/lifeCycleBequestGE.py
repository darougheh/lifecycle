"""
does GE computations with bequest model
"""

import lifeCycleBequest as bequest
import numpy as np
import scipy.optimize as optimize
import matplotlib.pyplot as plt
plt.interactive(False)

import copy
from scipy.interpolate import InterpolatedUnivariateSpline

def getStatistics(S, X, Y):
    def getGini(x):
        # requires all values in x to be zero or positive numbers,
        # otherwise results are undefined
        n = len(x)
        s = x.sum()
        r = np.argsort(np.argsort(-x))  # calculates zero-based ranks
        return 1 - (2.0 * (r * x).sum() + s) / (n * s)
    def getShares(x):

        percentiles = np.arange(5, 101, 5)
        xx = x.reshape((-1)).copy()
        xx.sort()
        xxPercentiles = [np.percentile(xx, p) for p in percentiles]
        indices = [np.argmin(np.abs(p - xx)) for p in xxPercentiles]
        return np.array([xx[index].cumsum()[0] for index in indices]) / xx[-1].cumsum()

    # variances of log earnings and consumption by age
    vS = np.log(Y).std(axis=0)
    vC = np.log(X - S).std(axis=0)
    # average earnings, consumption and wealth by age
    mY = Y.mean(axis=0)
    mC = (X - S).mean(axis=0)
    mX = S.mean(axis=0)
    # gini coefficients for earnings and wealth
    gY = getGini(Y.reshape((-1)))
    gX = getGini(S.reshape((-1)))
    # shares of income, consumption and wealth by quintiles of wealth distribution
    incomeShares = getShares(Y)
    consumptionShares = getShares(X-S)
    wealthShares = getShares(S)

    return {'vS': vS, 'vC':vC, 'mY':mY, 'mC':mC, 'mX':mX, 'gY':gY, 'gX':gX,
            'incomeShares':incomeShares, 'consumptionShares':consumptionShares, 'wealthShares':wealthShares}

def getKandW(r, Param):
    """
    solves K, w, for given interest rate, from firms' problem
    :param r: interest rate
    :param Param: parameters
    :return: Tuple: (KDemand, w)
    """
    # with deterministic death, we have each generation equally represented:
    lBar = Param.G.cumprod().mean()
    #g = Param.G.copy()
    #g[Param.TRet:] = 1
    #lBar = g.cumprod().mean()
    KDemand = (Param.alpha / (r + Param.delta)) ** (1 / (1 - Param.alpha)) * lBar
    w = (1-Param.alpha) * (Param.alpha/(r + Param.delta))**(1./(1-Param.alpha))

    # enforce smoothness for weird r
    if w > 1e+6:
        print 'Warning: w hit upper bound: ' + str(w)
        w = 1e+6

    return KDemand, w

def solveCapitalSupply(r, w, theta, WFunction, method, Param):
    """
    solves for a(r), capital supply
    :param r: interest rate
    :param w: wages
    :param theta: death rates
    :param WFunction: bequest function
    :param method: method of solving the problem (solveValueFunction, endogenousGridPointMethod)
    :param Param: parameters
    :return: total capital
    """
    policies = bequest.solve(method, theta, WFunction, Param, r=r, w=w)
    S, X, Y = bequest.simulate(policies, Param, r=r, w=w)
    # i assume measure 1 of households
    totalSavings = S.mean()
    return totalSavings

def solveEquilibrium(theta, WFunction, method, Param):
    """
    solves for r: a(r) == k(r). Stores also in Param.r
    :param theta: death rates
    :param WFunction: bequest function
    :param method: method of solving the problem (solveValueFunction, endogenousGridPointMethod)
    :param Param: parameters
    :return: r equilibrium interest rate
    """
    def solveEquilibriumError(r, theta, WFunction, method, Param):
        KDemand, w = getKandW(r, Param)
        capitalSupply = solveCapitalSupply(r, w, theta, WFunction, method, Param)
        return capitalSupply -KDemand
    r, results = optimize.brentq(solveEquilibriumError, -Param.delta + 1e-6, 3, args=(theta, WFunction, method, Param),
                                 full_output=True, xtol=1e-6)
    # solveEquilibriumError(2, theta, WFunction, method, Param)
    KDemand, w = getKandW(r, Param)
    capitalSupply = solveCapitalSupply(r, w, theta, WFunction, method, Param)
    print np.abs(KDemand/capitalSupply - 1)
    if np.abs(KDemand/capitalSupply - 1) > 0.1:
        print 'Warning: Did not find a good solution for r given beta = ' + str(Param.beta) + \
              ' (error of ' + str(np.abs(KDemand/capitalSupply - 1)) + ' )'

    Param.r = r
    return r

def calibrateCapitalOutputRatio(target, theta, WFunction, method, Param):
    """
    Solves for beta : K(beta)/Y(beta) == target
    Note: Changes Param.beta
    :param theta: death rates
    :param WFunction: bequest function
    :param method: method of solving the problem (solveValueFunction, endogenousGridPointMethod)
    :param Param: parameters
    :return: optimal beta
    """
    def calibrateCapitalOutputRatioError(beta, target, theta, WFunction, method, Param):
        Param.beta = beta
        r = solveEquilibrium(theta, WFunction, method, Param)
        KDemand, w = getKandW(r, Param)
        lBar = Param.G.cumprod().mean()

        ratio = (KDemand/lBar)**(1-Param.alpha)
        return ratio-target

    if method == bequest.solveEndogenousGridPoints or method.__name__ == 'solveEndogenousGridPoints':
        upperBound = 0.999
    else:
        upperBound = 1.5
    #rtol =
    beta, output  = optimize.brentq(calibrateCapitalOutputRatioError, 0.5, 1.5,
                               args=(target, theta, WFunction, method, Param),
                               full_output=True)
    #calibrateCapitalOutputRatioError(Param.beta, target, theta, WFunction, method, Param)
    if beta >= 1:
        raise RuntimeWarning('calibrated beta >= 1: ' + str(beta))
    Param.beta = beta
    return beta

def question2(Param):
    theta = np.ones((Param.T,))
    theta[-1] = 0
    WFunctionPrime = bequest.WNoBequestPrime
    ParamEndog = copy.deepcopy(Param)
    r = solveEquilibrium(theta, WFunctionPrime, bequest.solveEndogenousGridPoints, ParamEndog)
    print r

def question3(Param):
    theta = np.ones((Param.T,))
    theta[-1] = 0
    WFunctionPrime = bequest.WNoBequestPrime
    method = bequest.solveEndogenousGridPoints

    ParamEndog = copy.deepcopy(Param)
    target = 3
    del Param

    beta = calibrateCapitalOutputRatio(target, theta, bequest.WNoBequestPrime, bequest.solveEndogenousGridPoints, ParamEndog)
    ParamEndog.beta = beta

    r = solveEquilibrium(theta, WFunctionPrime, method, ParamEndog)
    ParamEndog.r = r
    KDemand, w = getKandW(r, ParamEndog)
    policies = bequest.solve(method, theta, WFunctionPrime, ParamEndog, r=r, w=w)
    S, X, Y = bequest.simulate(policies, ParamEndog, r=r, w=w)
    results = getStatistics(S, X, Y)

    # age graph
    ageAxis = np.arange(21, 101)

    fig1 = plt.figure()
    plt.plot(ageAxis, results['vS'], 'r', label='Earnings')
    plt.plot(ageAxis, results['vC'], label='Consumption')
    plt.legend()
    plt.legend(loc=2)


    fig2 = plt.figure()
    plt.plot(ageAxis, results['mY'], 'r', label='Income')
    plt.plot(ageAxis, results['mC'], label='Consumption')
    plt.plot(ageAxis, results['mX'], label='Savings')
    plt.legend()

    percentiles = np.arange(5, 101, 5)
    fig3 = plt.figure()
    plt.plot(percentiles, results['incomeShares'], label='Income')
    plt.plot(percentiles, results['consumptionShares'], label='Consumption')
    plt.plot(percentiles, results['wealthShares'], label='Wealth')
    plt.legend()

    fig1.savefig('ass2_q3_var.pdf')
    fig2.savefig('ass2_q3_mean.pdf')
    fig3.savefig('ass2_q3_shares.pdf')

    print 'Gini, Income: ' + str(results['gY'])
    print 'Gini, Assets: ' + str(results['gX'])


if __name__ == "__main__":

    Param = bequest.Parameters()
    Param.beta = 0.96
    Param.sigma = 0.2
    Param.varN = 0.0106
    Param.varV = 0.0738
    Param.alpha = 0.36
    Param.delta = 0.05


    question2(Param)
    question3(Param)


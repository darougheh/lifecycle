"""
Aggregate risk in an incomplete-markets, life-cycle pro- duction economy
"""
import numpy as np
import numpy.random as random
random.seed(1)
import matplotlib.pyplot as plt
import scipy.interpolate as interpolate
interp1d = interpolate.interp1d
import numpy.polynomial.hermite as hermite
import pandas as pd
import statsmodels.formula.api as sm
import statsmodels.api as api


import lifeCycleRetirement as retirement
import lifeCycleBequest as bequest
import lifeCycleGE as GE


WNoBequestPrime = bequest.WNoBequestPrime
cOfFPrime = bequest.cOfFPrime
fPrime = bequest.fPrime

class Parameters(retirement.Parameters):
    """
    extends base parameters
    """
    def __init__(self):

        self.rhoUI = 0.5

        super(self.__class__, self).__init__()

        self.sMin = 0.1
        self.sMax = 20
        #self.nS = 10
        self.setGrids()
        # find corresponding tauSS:

        # Parameters: kPrime = alpha1 + alpha2 * k
        # initial guess: identity
        self.alpha1 = np.array([0.095, 0.095])
        self.alpha2 = np.array([0.962, 0.965])

    def setGrids(self):
        '''
        creates grids, and stores them in the object. Does NOT call bequest.parameters.setGrids()
        :return: void
        '''
        self.nS = 30
        # aggregate capital grid

        self.kGrid = np.linspace(30, 90, 5)
        self.kGrid = np.hstack((15, self.kGrid, 120))
        self.nK = len(self.kGrid)

        # savings grid
        self.sGrid = np.zeros((self.nS,))
        self.sGrid[0] = self.sMin
        for i in range(1, self.nS):
            self.sGrid[i] = self.sGrid[i-1] + (self.sMax - self.sGrid[i-1])/((self.nS - i)**self.spacing)

        # cash on hand grid
        self.xGrid = np.zeros((self.nX))
        self.xGrid[0] = self.xMin
        for i in range(1, self.nX):
            self.xGrid[i] = self.xGrid[i - 1] + (self.xMax - self.xGrid[i-1]) / ((self.nX - i) ** self.spacing)

        # new Z, V grids:
        self.zGrid = np.array([0.98, 1.02])
        self.pZ = np.array([[0.9, 0.1], [0.05, 0.95]])
        self.nZ = 2
        self.vGrid = np.array([self.rhoUI, 1])

        self.pV = np.array([[0.1, 0.9], [0.06, 0.94]])
        self.nV = 2

        # N grid: as before
        self.muN = -self.varN*0.5
        nodes, weights = hermite.hermgauss(self.nY)
        self.nGrid = np.exp(np.sqrt(2 * self.varN) * nodes + self.muN)
        self.pN = weights / np.pi ** 0.5

        self.nN = self.nY
        self.X, self.S, = np.meshgrid(self.xGrid, self.sGrid, indexing='ij')

        self.V, self.N, self.Z = np.meshgrid(self.vGrid, self.nGrid, self.zGrid, indexing='ij')

def getTauUI(Z, Param):
    """

    :param Z: index variable 0 == bust, 1 == boom
    :param Param:
    :return:
    """
    numerator = Param.pV[Z, 0] * Param.rhoUI
    denominator = Param.pV[1,1]
    return numerator/denominator

def getPrices(K, Z, Param):
    lBar = Param.G.cumprod().mean() * Param.pV[Z, 1]
    # F_K - delta = r
    r = Param.alpha * Param.zGrid[Z] * (K/lBar)**(Param.alpha - 1) - Param.delta
    w = (1-Param.alpha) * Param.zGrid[Z] * (K/lBar)**(Param.alpha)
    return w, r

def interpolateKPrime(K, alpha1, alpha2):
    logKPrime = alpha1 + alpha2*np.log(K)
    return np.exp(logKPrime)

def interp2d(xResult, zGrid, cResult, extrapolateSecondAxis=False, xMax = None, zMax = None):
    """
    adds large boundaries to grids, returns interpolater that doesnt need to extrapolate
    :param extrapolateSecondAxis: whether to extrapolate on z-axis
    :param xMax: maximum x value for upper boundary
    :param zMax: maximum z value for upper boundary
    :return: interpolate.LinearNDInterpolator
    """
    if xMax is None:
        xMax = 10000
    if zMax is None:
        zMax = 1000

    if extrapolateSecondAxis == False:
        xx = np.zeros((xResult.shape[0] + 2, xResult.shape[1]))
        xx[1:-1, :] = xResult
        cc = np.zeros(xx.shape)
        cc[1:-1, :] = cResult
        xx[-1, :] = xMax  # "large"
        cc[-1, :] = cc[-2, :] + (cc[-2, :] - cc[-3, :]) / (xx[-2, :] - xx[-3, :]) * (xx[-1, :] - xx[-2, :])
        xx[0, :] = 0
        cc[0, :] = 0
        zz = zGrid + np.zeros(xx.shape)
    else:
        xx = np.zeros((xResult.shape[0] + 2, xResult.shape[1]+2))
        xx[1:-1, 1:-1] = xResult
        cc = np.zeros(xx.shape)
        cc[1:-1, 1:-1] = cResult
        zz = np.zeros(zGrid.shape[0]+2)
        zz[1:-1] = zGrid
        zz[0] = -1000
        zz[-1] = zMax

        xx[-1, :] = xMax + zz # "large", need
        xx[0, :] = 0

        xx[:, 0] = xx[:, 1] + (xx[:, 2] - xx[:, 1]) / (zz[2] - zz[1]) * (zz[1] - zz[0])
        xx[:, -1] = xx[:, -2] + (xx[:, -2] - xx[:, -3]) / (zz[-2] - zz[-3]) * (zz[-1] - zz[-2])


        cc[-1, :] = cc[-2, :] + (cc[-2, :] - cc[-3, :]) / (xx[-2, :] - xx[-3, :]) * (xx[-1, :] - xx[-2, :])
        cc[0, :] = 0

        # [0,0] corner has x=0, c=0. cannot extrapolate
        # if no descent at all, just copy:
        if xResult.var(axis=1).max() < 1e-6 and False:
            cc[:, 0] = cc[:, 1]
            xx[:, 0] = xx[:, 1]
            cc[:, -1] = cc[:, -2]
            xx[:, -1] = xx[:, -2]
        else:
            # if no descent in z-axis, over last 2 entries, take last 3 entries for approximation of fPrime
            isZero = xx[:, 2] - xx[:, 1] == 0
            nZero = isZero == False
            isZero[0] = 0# ignore 0-0 corner with 0 consumption
            nZero[0] = 0
            cc[1:, 0] = cc[1:, 1]
            cc[nZero, 0] += (cc[nZero, 2] - cc[nZero, 1]) / (xx[nZero, 2] - xx[nZero, 1]) * (xx[nZero, 1] - xx[nZero, 0])
            #cc[isZero, 0] += ( (cc[isZero, 3] - cc[isZero, 1]) / (xx[isZero, 3] - xx[isZero, 1])
            #                 * (xx[isZero, 1] - xx[isZero, 0]) )
            cc[isZero, 0] = cc[isZero, 1]

            isZero = xx[:, -2] - xx[:, -3] == False
            nZero = isZero == False
            isZero[0] = 0# ignore 0-0 corner with 0 consumption
            nZero[0] = 0
            cc[1:, -1] = cc[1:, -2]
            cc[nZero, -1] += ( (cc[nZero, -2] - cc[nZero, -3]) / (xx[nZero, -2] - xx[nZero, -3])
                               * (xx[nZero, -1] - xx[nZero, -2]) )
            #cc[isZero, -1] += ( (cc[isZero, -2] - cc[isZero, -4]) / (xx[isZero, -2] - xx[isZero, -4])
            #                  * (xx[isZero, -1] - xx[isZero, -2]) )


        zz = zz + np.zeros(xx.shape)

    return interpolate.LinearNDInterpolator((xx.flatten(), zz.flatten()), cc.flatten(),
                                            rescale=True)

def solveEndogenousGridPoints(Param):
    """
    solves using endogenous grid point method. Income from retirement = rho * J_TRet-1 * w
    :param theta: T-size vector, float, likelihood of survival
    :param WFunction: Bequest function
    :param Param: Parameters object
    :param r: Optional, interest rate. If none provided, will fetch from Param object
    :return: C,X: objects of shape (nS, nZ, nT) that contain optimal consumption for corresponding x grid
    """

    sGrid = Param.sGrid.copy()
    if sGrid[0] == 0:
        sGrid = sGrid[1:]
    nS = len(sGrid)

    cResult = np.zeros((nS, Param.nZ, Param.nK, Param.T))
    xResult = np.zeros((nS, Param.nZ, Param.nK, Param.T))

    # Last period: solve optimal savings share gamma (see notes)
    xResult[..., -1] = np.linspace(0.01, 10, nS).reshape((-1,1,1)).repeat(Param.nZ, axis=1).repeat(Param.nK, axis=2)
    cResult[..., -1] = xResult[..., -1]

    tauIU = getTauUI(np.array([0, 1]), Param)

    # iterate over t. Structured similar as matlab original
    for t in np.arange(Param.T-1, dtype=int)[::-1]:
        for k in np.arange(Param.nK):
            K = Param.kGrid[k]

            for z1 in np.arange(Param.nZ):

                gothicV = np.zeros((nS))

                for z2 in np.arange(Param.nZ):
                    alpha1, alpha2 = Param.alpha1[z1], Param.alpha2[z2]
                    KPrime = interpolateKPrime(K, alpha1, alpha2)
                    w, r = getPrices(K, z1, Param)
                    wPrime, rPrime = getPrices(KPrime, z2, Param)
                    interpolateMe = interp2d(xResult[:, z2, :, t+1],
                        Param.kGrid,
                        cResult[:, z2, :, t+1],
                        extrapolateSecondAxis=True,
                        xMax = xResult.max() * 100,
                        zMax = KPrime * 1.5)
                    if t+1 < Param.TRet:
                        for i in np.arange(Param.nN):
                            for j in np.arange(Param.nV):
                                savingsIncome = sGrid * (1+r)/(Param.G[t+1]*Param.nGrid[i])
                                if j == 0:
                                    net = 1-Param.tauSS
                                else:
                                    net = 1 - Param.tauSS - tauIU[z1]

                                wageIncome = Param.vGrid[j] * wPrime * net

                                xPrime = savingsIncome + wageIncome
                                cPrime = interpolateMe(xPrime, KPrime)

                                prob = Param.pN[i]* Param.pZ[z1, z2] * Param.pV[z2, j]
                                uPrime = (Param.G[t+1] * Param.nGrid[i])**(-Param.sigma) * fPrime(cPrime, Param)
                                gothicV +=  prob * Param.beta * (1+r) * uPrime

                    else:
                        xPrime = sGrid * (1+r) + Param.rho*wPrime
                        cPrime = interpolateMe(xPrime, KPrime)
                        gothicV += Param.beta * (1+r) * Param.pZ[z1, z2] *  fPrime(cPrime, Param)

                cResult[:, z1, k, t] = cOfFPrime(gothicV, Param)
                xResult[:, z1, k, t] = cResult[:, z1, k, t] + sGrid
    Param.xGrid = xResult
    sResult = xResult - cResult
    return cResult, sResult

def simulateNested(policies, S, X, w, Z, Param):
    """
    :param policies:
    :param S: Shape (Param.T, Param.I
    :param Param:
    :return:
    """
    # first: Draw state

    # permanent
    N = np.ones((Param.TT, Param.T, Param.I))
    N[:, 0:Param.TRet, :] = random.lognormal(Param.muN, np.sqrt(Param.varN), (Param.TT, Param.TRet, Param.I ) )

    # transitory
    V = np.ones((Param.TT, Param.T, Param.I))
    for t in np.arange(0, Param.T):
        V[t, ...] = random.choice(np.array([0, 1]), (Param.T, Param.I,), p=Param.pV[Z[t],:])
    # either 0.5 if unemployed, or 1
    V[V == 0] = 0.5

    # permanent income component
    P = np.ones((Param.TT, Param.T, Param.I))
    P[:, 0, :] = Param.G[0] * N[:,0, :]

    for j in np.arange(1,Param.TRet):
        P[:, j, :] = Param.G[j] * P[:, j-1, :] * N[:, j, :]

    V[:, Param.TRet:] = Param.rho

    # deterministric transition
    tFirstBoom = Z.argmax()
    tFirstBust = Z.argmin()
    V[Z == 0, ...] = V[tFirstBust, ...][np.newaxis, ...]
    V[Z == 1, ...] = V[tFirstBoom, ...][np.newaxis, ...]
    P[1:, ...] = P[0, ...][np.newaxis, ...]
    N[1:, ...] = N[0, ...][np.newaxis, ...]
    normX = np.zeros((Param.TT, Param.T, Param.I))
    normS = np.zeros(normX.shape)
    normX[:, 0, :] = V[:, 0, :] * w # cash-on-hand for first-years

    # overwrite t=0 with steady state data (without aggr. risk)
    normXOld = X.T/P[0, ...]
    normX[0, ...] = normXOld

    ww = np.zeros((Param.TT))
    rr = np.zeros(ww.shape)
    KK = np.zeros(ww.shape)

    tauUI = getTauUI(np.array([0, 1]), Param)
    tauLong = tauUI[Z[t]]
    Y = P*V*w*(1-Param.tauSS - tauLong)

    c, s = policies # shape nS, nZ, nT
    for t in np.arange(0, Param.TT-1):
        if t == 0:
            K = (S.T/P[0, ...]).sum()/(Param.T * Param.I) # normalize savings from t-1
        else:
            K = (P[t-1, ...]*normS[t-1, ...]).sum()/(Param.I * Param.T)
        KNext = KPrimeOFK(K, Z[t], Param)

        w, r = getPrices(K, Z[t], Param)
        wNext, rNext = getPrices(KNext, Z[t+1], Param)

        ww[t], rr[t] = w, r
        KK[t] = K

        for j in np.arange(Param.T - 1):

            interpolateMe = interp2d(
                Param.xGrid[:, Z[t], :, j],
                Param.kGrid,
                s[:, Z[t], :, j],
                extrapolateSecondAxis=True,
                xMax=normX[t, j, :].max() + Param.xGrid[:, Z[t], :, j].max(),
                zMax = K*1.5)
            normS[t, j, :] = interpolateMe(normX[t, j, :].reshape(-1), K).reshape(normX[t, j, :].shape)
            if any(normS[t, j, :] < 0) or any(np.isnan(normS[t, j, :])):
                raise BaseException
            if Param.G[j+1] > 0:
                normX[t + 1, j + 1, :] = (1+r) * normS[t, j, :] * (Param.G[j+1] * N[t + 1, j + 1, :])**(-1)
                normX[t + 1, j + 1, :] += V[t + 1, j + 1, :]*wNext*(1-Param.tauSS - tauUI[Z[t]])
            else:
                normX[t + 1, j + 1, :] = (1+r) * normS[t, j, :] + Param.rho * wNext

    X = normX * P
    S = normS * P
    return S, X, Y

def KPrimeOFK(K, Z, Param):
    """
    creates forecast of capital given current parameters
    :param K:
    :param Z: 0 or 1 for bust, boom
    :param Param:
    :return:
    """
    logKPrime = Param.alpha1[Z] + Param.alpha2[Z]*np.log(K)
    return np.exp(logKPrime)

def estimate(df, Param):
    """
    estimates law of motion for capital given time series
    :param df: pd.DataFrame time series
    :param Param: Parameters object
    :return: np.array, np.array, sm.result, sm.result (order: bust, boom)
    """
    resultBust = sm.ols(formula='lkPrime ~ lk', data=df[df.boom == False]).fit()
    resultBoom = sm.ols(formula='lkPrime ~ lk', data=df[df.boom == True]).fit()

    alpha1 = np.array([resultBust.params['Intercept'], resultBoom.params['Intercept']])
    alpha2 = np.array([resultBust.params['lk'], resultBoom.params['lk']])
    return alpha1, alpha2, resultBust, resultBoom

def calibrateEstimation(Param):
    """
    iterates until law of motion converges
    :param Param: Parameters object
    :return: S, X, Y, Z, result1, result2, policies1, policies2 (bust, boom)
    """

    ParamSS = retirement.Parameters(retirementSystem='PAYGO')
    ParamSS.I = Param.I # as many agents
    ParamSS.r = retirement.solveEquilibrium(ParamSS)
    KSS, wSS = GE.getKandW(ParamSS.r, ParamSS)
    policiesSS = retirement.computePolicies(ParamSS.r, wSS, ParamSS)
    SSS, XSS, YSS = retirement.simulate(ParamSS.r, wSS, policiesSS, ParamSS)


    Z = np.ones((Param.TT,)).astype(int) * (-1)
    Z[0] = 1
    for z in np.arange(len(Z)-1):
        Z[z+1] = random.choice(np.array([0, 1]), (1,), p=Param.pZ[Z[z]])

    alpha = np.ones((100, 2, 2))

    i = 0
    while True:

        # compute policies
        policies = solveEndogenousGridPoints(Param)
        c,s = policies

        if (np.diff(c, axis=0) < 0).any() and False:
            raise BaseException

        # simulate
        S, X, Y = simulateNested(policies, SSS, XSS, wSS, Z, Param)


        # compute aggregate capital, drop 150 periods
        K = S.sum(axis=-1).sum(axis=-1)/(Param.T * Param.I)
        dropIndex = 150
        K = K[dropIndex:-2]
        KPrime = K[1:]
        K = K[:-1]

        ZZ = Z[dropIndex:-2]
        ZZ = ZZ[1:]

        # prepare regression
        df = pd.DataFrame(columns=['k', 'kPrime', 'lk', 'lkPrime', 'boom'])
        df.k = K
        df.kPrime = KPrime
        df.lk = np.log(df.k)
        df.lkPrime = np.log(df.kPrime)
        df.boom = ZZ # Z is 0/1 index. boom == 1

        # get new parameters
        Param.alpha1, Param.alpha2, result1, result2 = estimate(df, Param)
        alpha[i, 0, :] = Param.alpha1
        alpha[i, 1, :] = Param.alpha2
        print i, np.abs(alpha[i, :] - alpha[i-1,:]).max()

        # convergence?
        if np.abs(alpha[i, :] - alpha[i-1,:]).max() < 1e-02:
            return S[dropIndex:-2, ...], X[dropIndex:-2, ...], Y[dropIndex:-2, ...], Z[dropIndex:-2, ...], result1, result2, policies

        i+=1
        if i > 100:
            raise BaseException('Did not converge after ' + str(i) + ' iterations. Stopping now.')

def question2():
    """
    runs all the stuff, stores plots.
    :return: nothing
    """
    Param1 = Parameters()
    Param1.rhoUI = 0.5
    Param1.setGrids()
    Param1.I = 500
    Param1.TT = 1000

    Param2 = Parameters()
    Param2.rhoUI = 0.8
    Param2.setGrids()
    Param2.I = 500
    Param2.TT = 1000


    S1, X1, Y1, Z1, resultBust1, resultBoom1, policies1 = calibrateEstimation(Param1)
    S2, X2, Y2, Z2, resultBust2, resultBoom2, policies2 = calibrateEstimation(Param2)

    # statistics

    stat1 = GE.getStatistics(S1, X1, Y1)
    stat2 = GE.getStatistics(S2, X2, Y2)

    stats = {'50':stat1, '80':stat2}

    N = 5 # 5 percentiles
    ind = np.arange(N)+1  # the x locations for the groups
    width = 0.25  # the width of the bars
    myList = ['incomeShares', 'consumptionShares', 'wealthShares']
    for item in myList:
        fig, ax = plt.subplots()
        rects1 = ax.bar(ind - width, stat1[item], width, color='#554444')#, align='center')
        rects2 = ax.bar(ind, stat2[item], width, color='#A8A8A8')#, align='center')

        # add some text for labels, title and axes ticks
        ax.set_ylabel('Shares')
        ax.set_xticks(ind)
        ax.set_xticklabels(('0', '0.2', '0.4', '0.6', '0.8', '1'))
        plt.xlim(0.5, 5.5)
        plt.ylim(0, 0.7)
        ax.legend((rects1[0], rects2[0]), ('US', 'Sweden'), loc=2)
        plt.savefig('../content/ass3q2' + item + '.pdf', bbox_inches="tight", pad_inches=0)
        plt.close()

    df = pd.DataFrame(columns=['Gini', 'K'],
                  index=['US', 'Sweden'])
    df.loc['US', :] = np.array([stat1['gX'], (S1.sum(axis=-1).sum(axis=-1)/(Param1.T*Param1.I)).mean()])
    df.loc['Sweden', :] = np.array([stat2['gX'], (S2.sum(axis=-1).sum(axis=-1)/(Param1.T*Param1.I)).mean()])
    df.to_latex('../content/ass3q2stats.tex')

    K = S1.sum(axis=-1).sum(axis=-1)/(Param1.T*Param1.I)
    fig, ax = plt.subplots(1,  1)
    kk = K[200:300]
    zz = Z1[200:300]
    ax.plot(kk)
    edges = tuple(np.where(zz[:-1] != zz[1:])[0] + 1) + (len(zz),)
    colors = ['w', '#787878']
    labels = ['Boom', 'Recession']
    Boom = False
    x1,x2,y1,y2 = plt.axis()
    plt.axis((x1,x2,0, 100))

    for ii in np.arange(len(edges)-1):
        Boom = Boom == False
        ax.axvspan(edges[ii], edges[ii + 1], facecolor=colors[Boom],
                   label=labels[Boom], alpha=0.3)
    plt.savefig('../content/ass3q2series.pdf')
    plt.close()

    fig, ax = plt.subplots()
    fig = api.graphics.plot_fit(resultBoom1, 1, ax=ax)
    fig.savefig('../content/ass3q2fit.pdf')
    plt.close()

if __name__ == '__main__':
    question2()


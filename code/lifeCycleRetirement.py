"""
Social security reform in an incomplete-markets, life-cycle production economy
"""
import numpy as np
import numpy.random as random
from scipy.interpolate import InterpolatedUnivariateSpline
import matplotlib.pyplot as plt

import lifeCycleBequest as bequest
cOfFPrime = bequest.cOfFPrime
fPrime = bequest.fPrime
import copy
import lifeCycleGE as GE
import scipy.stats as stats
norm = stats.norm

import scipy.interpolate as interpolate
import scipy.optimize as optimize
plt.interactive(False)
import pandas as pd

import gc
import argparse

class Parameters(bequest.Parameters):
    """
    extends base parameters
    """
    def __init__(self, retirementSystem=None):

        self.retirementSystem = retirementSystem  # must be either 'PAYGO', 'REDISTRIBUTIVE' or 'HYBRID'

        self.pMin = 1
        self.pMax = 300


        self.nP = 50
        self.nZ = 10
        self.ZMin = 0
        self.ZMax = 100


        # environment
        self.T = np.float64(80)
        self.I = 10000  # number of individuals for simulation

        # preferences
        self.TRet = np.float64(45)  # period of retirement
        self.sigma = np.float64(2)  # risk averison
        self.rho = np.float64(0.04)  # discount rate
        self.beta = 1 / (1 + self.rho)

        # grid points
        self.sMin = 1e-4
        self.sMax = 20
        self.spacing = 1.5
        self.nS = 250
        self.nS = 20
        self.nX = 200
        self.xMin = 0.001
        self.xMax = 30

        # shocks
        self.r = np.float64(0.02)  # interest rate
        self.varN = np.float64(0.0106)  # variance of permanent shocks
        self.varV = np.float64(0.0738)  # -- transitory shocks
        self.nY = np.float64(5)  # number of nodes in quadrature of income
        self.probV0 = np.float64(0.0005)  # probability of zero income: 0 => explicit
        self.probV0 = 0



        self.sigma = np.float64(2)
        self.varN = 0.0106
        self.varV = 0.0738
        self.alpha = 0.36
        self.delta = 0.1
        self.T = 65
        self.TRet = 45

        # life cycle profile of income(deterministic)
        self.G = np.array([
            15.5273036489961, 1.13087311215982, 1.05870874450936, 1.05467219182954, 1.05077711505543,
            1.04702205854521, 1.04340562226528, 1.03992646092071, 1.03658328312142, 1.03337485058359,
            1.03029997736546, 1.02735752913683, 1.02454642248183, 1.02186562423414, 1.01931415084419,
            1.01689106777780, 1.01459548894578, 1.01242657616384, 1.01038353864259, 1.00846563250697,
            1.00667216034486, 1.00500247078444, 1.00345595809984, 1.00203206184495, 1.00073026651485,
            0.999550101234814, 0.998491139476392, 0.997552998800479, 0.996735340627130, 0.996037870031881,
            0.995460335568447, 0.995002529117637, 0.994664285762371, 0.994445483688613, 0.994346044112305,
            0.994365931232058, 0.994505152207672, 0.994763757164433, 0.995141839223198, 0.995639534556274,
            0.996257022469168, 0.996994525508269, 0.997852309594495, 0.998830684183142, 0.999930002449943,
            0.682120000000000, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            dtype=np.float64)

        # add 15 more ones (age of 85 versus 100)
        self.G = np.hstack((self.G, np.ones((15,))))
        self.G[self.TRet:] = 0

        self.setGrids()

        self.tauSS = 0.153
        self.tauFF = 0.1
        self.tauPAYGO = self.tauSS - self.tauFF

        self.rho = self.computeRho()

    def computeRho(self):
        if self.retirementSystem == 'HYBRID':
            tax = self.tauFF
        else:
            tax = self.tauSS
        numerator = (self.G[:self.TRet].cumprod()*tax).sum()
        denominator = (self.T - self.TRet + 1) * self.G[:self.TRet].cumprod()[-1]
        # (J - JRet + 1) * Prod_j=1^JRet-1 Gj
        return numerator/denominator

    def setGrids(self):
        '''
        creates grids, and stores them in the object.
        :return: void
        '''
        bequest.Parameters.setGrids(self)
        if self.retirementSystem == 'HYBRID':
            self.zGrid = np.linspace(self.ZMin, self.ZMax, self.nZ)


        if self.retirementSystem == 'REDISTRIBUTIVE':
            self.pGrid = np.zeros(self.nP)
            self.pGrid[0] = self.pMin
            for i in range(1, self.nP):
                self.pGrid[i] = self.pGrid[i - 1]
                self.pGrid[i] += (self.pMax - self.pGrid[i - 1]) / ((self.nP - i) ** self.spacing)
            self.pGrid = np.linspace(self.pMin, self.pMax, self.nP)

def computePoliciesPaygo(r, w, Param):
    """
    solves using endogenous grid point method. Income from retirement = rho * J_TRet-1 * w
    :return: C,S: objects of shape (nS, nT) that contain optimal consumption for corresponding xgrid
    """
    random.seed(1)

    sGrid = Param.sGrid.copy()
    if sGrid[0] == 0:
        sGrid = sGrid[1:]

    nS = len(sGrid)

    cResult = np.zeros((nS, Param.T))
    xResult = np.zeros((nS, Param.T))

    # Last period: solve optimal savings share gamma (see notes)
    xResult[:, -1] = np.linspace(0.01, 10, nS) # arbitrary grid
    cResult[:, -1] = xResult[:, -1]
    try:
        if r.shape[0] == Param.T:
            rPath = r.copy()
            wPath = w.copy()
            transition = True
        else:
            transition = False
    except (AttributeError, IndexError):
        transition = False
    # iterate over t. Structured similar as matlab original
    for t in np.arange(Param.T-1, dtype=int)[::-1]:
        if transition:
            r = rPath[t]
            w = wPath[t]
        # interpolator over c_t+1(x_t+1)
        interpolateMe = InterpolatedUnivariateSpline(
            np.hstack((0,xResult[:, t + 1])), np.hstack((0,cResult[:, t + 1])), k=1, ext=0)

        if t+1 < Param.TRet:
            gothicV = np.zeros((nS))
            for i in np.arange(Param.nN):
                for j in np.arange(Param.nV):
                    xPrime = sGrid * (1+r)/(Param.G[t+1]*Param.nGrid[i]) + Param.vGrid[j] * w * (1-Param.tauSS)
                    cPrime = interpolateMe(xPrime)
                    gothicV += ( Param.beta * (1+r) * Param.pN[i] * Param.pV[j] *
                        ( Param.G[t+1] * Param.nGrid[i])**(-Param.sigma) * fPrime(cPrime, Param))
        else:
            xPrime = sGrid * (1+r) + Param.rho*w

            cPrime = interpolateMe(xPrime)
            gothicV = Param.beta * (1+r) * fPrime(cPrime, Param)

        cResult[:,t] = cOfFPrime(gothicV, Param)
        xResult[:,t] = cResult[:,t] + sGrid

    Param.xGrid = xResult.copy()
    sResult = xResult - cResult
    return cResult, sResult

def computePoliciesRedistributive(r, w, Param):
    """
    solves using endogenous grid point method. Income from retirement = rho * J_TRet-1 * w
    :return: C,S: objects of shape (nS, nT) that contain optimal consumption for corresponding xgrid
    """
    random.seed(1)

    sGrid = Param.sGrid.copy()
    if sGrid[0] == 0:
        sGrid = sGrid[1:]

    nS = len(sGrid)

    cResult = np.zeros((nS, Param.nP, Param.T))
    xResult = np.zeros(cResult.shape)

    # Last period: solve optimal savings share gamma (see notes)
    xResult[:, :, -1] = np.linspace(0.01, 10, nS).reshape((-1, 1)).repeat(Param.nP, axis=1) # arbitrary grid
    cResult[:, :, -1] = xResult[:, :, -1]
    try:
        if r.shape[0] == Param.T:
            rPath = r.copy()
            wPath = w.copy()
            transition = True
        else:
            transition = False
    except AttributeError:
        transition = False
    # iterate over t. Structured similar as matlab original
    for t in np.arange(Param.T-1, dtype=int)[::-1]:
        if transition:
            r = rPath[t]
            w = wPath[t]

        if t+1 < Param.TRet:
            for p in np.arange(Param.nP):
                gothicV = np.zeros((nS))

                for i in np.arange(Param.nN):
                    for j in np.arange(Param.nV):
                        pPrime = Param.pGrid[p]*Param.G[t+1]*Param.nGrid[i]
                        xPrime = sGrid * (1+r)/(Param.G[t+1]*Param.nGrid[i]) + Param.vGrid[j] * w * (1-Param.tauSS)
                        interpolateMe = interp2d(xResult[..., t + 1], Param.pGrid, cResult[..., t + 1],
                                 extrapolateSecondAxis=True,
                                 xMax = xResult[:, :, t+1].max() + xPrime.max(),
                                 zMax = Param.pGrid.max() * 2)
                        cPrime = interpolateMe(xPrime, pPrime)
                        if any(np.isnan(cPrime)):
                            raise BaseException
                        gothicV += ( Param.beta * (1+r) * Param.pN[i] * Param.pV[j] *
                            ( Param.G[t+1] * Param.nGrid[i])**(-Param.sigma) * fPrime(cPrime, Param))

                cResult[:, p, t] = cOfFPrime(gothicV, Param)
                xResult[:, p, t] = cResult[:, p, t] + sGrid
        else:
            for p in np.arange(Param.nP):

                xPrime = sGrid * (1 + r) + Param.rho * w / Param.pGrid[p] # P' == P

                interpolateMe = interp2d(xResult[..., t + 1], Param.pGrid,
                    cResult[..., t + 1],
                    extrapolateSecondAxis=False,
                    xMax = xResult[:, :, t+1].max() + xPrime.max())

                cPrime = interpolateMe(xPrime,Param.pGrid[p])
                gothicV = Param.beta * (1+r) * fPrime(cPrime, Param)

                cResult[:, p, t] = cOfFPrime(gothicV, Param)
                xResult[:, p, t] = cResult[:, p, t] + sGrid

    Param.xGrid = xResult.copy()
    sResult = xResult - cResult
    return cResult, sResult

def computePoliciesHybrid(r, w, Param):
    """
    solves using endogenous grid point method. Income from retirement = rho * J_TRet-1 * w
    :return: C,S: objects of shape (nS, nT) that contain optimal consumption for corresponding xgrid
    """
    random.seed(1)

    sGrid = Param.sGrid.copy()
    # chance of 0 income. hence remove 0 grid point
    if sGrid[0] == 0:
        sGrid = sGrid[1:]

    nS = len(sGrid)

    cResult = np.zeros((nS, Param.nZ, Param.T))
    xResult = np.zeros((nS, Param.nZ, Param.T))

    # Last period: solve optimal savings share gamma (see notes)
    xResult[:, :, -1] = np.linspace(0.01, 10, nS).reshape((-1, 1)).repeat(Param.nZ, axis=1)
    cResult[:, :, -1] = xResult[:, :, -1]
    try:
        if r.shape[0] == Param.T:
            rPath = r.copy()
            wPath = w.copy()
            transition = True
        else:
            transition = False
    except (AttributeError, IndexError):
        transition = False
    # iterate over t. Structured similar as matlab original
    for t in np.arange(Param.T-1, dtype=int)[::-1]:
        if transition:
            r = rPath[t]
            w = wPath[t]

        interpolateMe = interp2d(xResult[..., t + 1], Param.zGrid, cResult[..., t + 1],
                                 extrapolateSecondAxis=True,
                                 xMax = xResult[:, :, t+1].max() + 10*w +
                                        (Param.zGrid.max()+sGrid.max()) * (1 + r) * 10
                                 )
        if t < Param.TRet - 1:
            for z in np.arange(Param.nZ):
                gothicV = np.zeros((nS))
                for i in np.arange(Param.nN):
                    for vPrime in np.arange(Param.nV):
                        zPrime = Param.zGrid[z] * (1 + r)/(Param.G[t+1]*Param.nGrid[i])
                        zPrime += Param.tauPAYGO * w * Param.vGrid[vPrime]/(Param.G[t + 1] * Param.nGrid[i])

                        if zPrime > Param.zGrid.max():
                            zPrime = Param.zGrid.max()

                        xPrime = sGrid * (1+r)/(Param.G[t+1]*Param.nGrid[i])
                        xPrime += Param.vGrid[vPrime] * w * (1-Param.tauSS)
                        cPrime = interpolateMe(xPrime, zPrime)
                        gothicV += ( Param.beta * (1+r) * Param.pN[i] * Param.pV[vPrime] *
                            ( Param.G[t+1] * Param.nGrid[i])**(-Param.sigma) * fPrime(cPrime, Param))

                cResult[:, z, t] = cOfFPrime(gothicV, Param)
                xResult[:, z, t] = cResult[:, z, t] + sGrid
        elif t == Param.TRet - 1:
            for z in np.arange(Param.nZ):
                gothicV = np.zeros((nS))
                zPrime = Param.zGrid[z] * (1 + r)
                zPrime += Param.tauPAYGO * w
                xPrime = sGrid * (1 + r) + Param.rho *  w
                xPrime += zPrime
                zPrime = 0

                cPrime = interpolateMe(xPrime, zPrime)
                gothicV += Param.beta * (1 + r) * fPrime(cPrime, Param)

                cResult[:, z, t] = cOfFPrime(gothicV, Param)
                xResult[:, z, t] = cResult[:, z, t] + sGrid
        else:

            xPrime = sGrid * (1 + r) + Param.rho * w
            zPrime = 0
            if zPrime > Param.zGrid.max():
                zPrime = Param.zGrid.max()
            cPrime = interpolateMe(xPrime, zPrime)
            gothicV = Param.beta * (1 + r) * fPrime(cPrime, Param)
            cResult[:, :, t] = cOfFPrime(gothicV, Param).reshape((-1, 1)).repeat(Param.nZ, axis=1)
            xResult[:, :, t] = cResult[:, :, t] + sGrid[:, np.newaxis]

    Param.xGrid = xResult.copy()
    sResult = xResult - cResult
    return cResult, sResult

def computePolicies(r,w, Param):

    method = {
        'HYBRID': computePoliciesHybrid,
        'PAYGO': computePoliciesPaygo,
        'REDISTRIBUTIVE': computePoliciesRedistributive}

    try:
        return method[Param.retirementSystem](r, w, Param)
    except OSError:
        print 1

def simulatePaygo(r, w, policies, Param):
    """
    Simulates the model
    :param policies: Consumption and Savings Policies
    :param Param: Parameters object
    :return: S, X, Y: De-normalized savings, cash-on-hand, and income for simulated individuals,
    all of shape (I, T)
    """

    c,s = policies

    # permanent
    N = np.ones((Param.I, Param.T))
    N[:,0:Param.TRet] = random.lognormal(Param.muN, np.sqrt(Param.varN), (Param.I, Param.TRet ) )

    # transitory
    V = np.ones((Param.I, Param.T))
    V[:, 0:Param.TRet] = random.lognormal(Param.muV, np.sqrt(Param.varV), (Param.I, Param.TRet))

    # permanent income component
    P = np.ones((Param.I, Param.T))
    P[:,0] = Param.G[0] * N[:,0]
    for t in np.arange(1,Param.T):
        P[:,t] = Param.G[t] * P[:,t-1] * N[:,t]

    # not sure if I need this
    P[:, Param.TRet:] = P[:, Param.TRet-1].reshape((-1,1))
    V[:, Param.TRet:] = 1

    R = np.ones(Param.T) * Param.rho
    R[:Param.TRet] = 1 - Param.tauSS
    Y = P*V*w*R[np.newaxis, :]

    normX = np.zeros((Param.I, Param.T) ) # evolution of states: normalized cash on hands
    normS = np.zeros((Param.I, Param.T)) # simulated behavior: normalized savings

    normX[:,0] = V[:,0]

    for t in np.arange(Param.T-1):
        interpolateMe = InterpolatedUnivariateSpline(
            np.hstack((0, Param.xGrid[:, t].copy())),
            np.hstack((0, s[:, t])),
            k=1)

        normS[:,t] = interpolateMe(normX[:,t].reshape(-1)).reshape(normX[:,t].shape)
        if t + 1 < Param.TRet:
            normX[:,t+1] = (1+r) * normS[:,t] * (Param.G[t+1] * N[:,t+1])**(-1) + V[:,t+1]*w*(1-Param.tauSS)
        else:
            normX[:,t+1] = (1+r) * normS[:,t] + Param.rho * w



    S = normS * P
    X = normX * P

    return S, X, Y

def simulateRedistributive(r, w, policies, Param):

    c,s = policies

    # permanent
    N = np.ones((Param.I, Param.T))
    N[:,0:Param.TRet] = random.lognormal(Param.muN, np.sqrt(Param.varN), (Param.I, Param.TRet ) )

    # transitory
    V = np.ones((Param.I, Param.T))
    V[:, 0:Param.TRet] = random.lognormal(Param.muV, np.sqrt(Param.varV), (Param.I, Param.TRet))

    # permanent income component
    P = np.ones((Param.I, Param.T))
    P[:,0] = Param.G[0] * N[:,0]
    for t in np.arange(1,Param.T):
        P[:,t] = Param.G[t] * P[:,t-1] * N[:,t]

    # not sure if I need this
    P[:, Param.TRet:] = P[:, Param.TRet-1].reshape((-1,1))
    R = np.ones(Param.T)
    R[:Param.TRet] = 1-Param.tauSS
    R[Param.TRet:] = Param.rho


    Y = P*V*R*w
    Y[:, Param.TRet:] = P.mean() * V[:, Param.TRet:] * w * Param.rho


    normX = np.zeros((Param.I, Param.T) ) # evolution of states: normalized cash on hands
    normS = np.zeros(normX.shape) # simulated behavior: normalized savings
    normX[:, 0] = V[:,0]*R[0]


    for t in np.arange(Param.T-1):

        interpolateMe = interp2d(Param.xGrid[..., t], Param.pGrid, s[..., t], extrapolateSecondAxis=True,
            xMax = normX[:, t].max() + 10)

        normS[:, t] = interpolateMe(
            normX[:, t].reshape(-1), P[:, t].reshape(-1)).reshape(normX[:, t].shape)

        # retirement model: G_t+1 == 0
        if t + 1< Param.TRet:
            normX[:, t + 1] =  (1+r) * normS[:, t] / (Param.G[t+1] * N[:,t+1])
            normX[:, t + 1] += V[:, t + 1]*w*(1-Param.tauSS)

        elif t+1 >= Param.TRet:
            normX[:, t + 1] = (1 + r) * normS[:, t] + Param.rho * w/P[:, t + 1]
        else:
            raise BaseException('this should never happen')


    S = normS * P
    X = normX * P


    return S, X, Y

def simulateHybrid(r, w, policies, Param):
    """
    Simulates the model
    :param policies: Consumption and Savings Policies
    :param Param: Parameters object
    :return: S, X, Y: De-normalized savings, cash-on-hand, and income for simulated individuals,
    all of shape (I, T)
    """

    c,s = policies

    # permanent
    N = np.ones((Param.I, Param.T))
    N[:,0:Param.TRet] = random.lognormal(Param.muN, np.sqrt(Param.varN), (Param.I, Param.TRet ) )

    # transitory
    V = np.ones((Param.I, Param.T))
    V[:, 0:Param.TRet] = random.lognormal(Param.muV, np.sqrt(Param.varV), (Param.I, Param.TRet))

    # permanent income component
    P = np.ones((Param.I, Param.T))
    P[:,0] = Param.G[0] * N[:,0]
    for t in np.arange(1,Param.T):
        P[:,t] = Param.G[t] * P[:,t-1] * N[:,t]

    # not sure if I need this
    P[:, Param.TRet:] = P[:, Param.TRet-1].reshape((-1,1))
    R = np.ones(Param.T)
    R[:Param.TRet] = 1-Param.tauSS
    R[Param.TRet:] = Param.rho


    Y = P*V*R*w


    normX = np.zeros((Param.I, Param.T) ) # evolution of states: normalized cash on hands
    normS = np.zeros(normX.shape) # simulated behavior: normalized savings
    normZ = np.zeros(normX.shape)
    normX[:, 0] = V[:,0]*R[0]
    normZ[: ,0] = 0

    for t in np.arange(Param.T-1):

        interpolateMe = interp2d(Param.xGrid[..., t], Param.zGrid, s[..., t],
                                 extrapolateSecondAxis=True, xMax=normX[:, t].max() + Param.xGrid[..., t].max())

        normS[:, t] = interpolateMe(
            normX[:, t].reshape(-1), normZ[:, t].reshape(-1)).reshape(normX[:, t].shape)

        # retirement model: G_t+1 == 0
        if t + 1< Param.TRet:
            normX[:, t + 1] =  (1+r) * normS[:, t] / (Param.G[t+1] * N[:,t+1])
            normX[:, t + 1] += V[:, t + 1]*w*(1-Param.tauSS)

            normZ[:, t + 1] =  (1+r) * normZ[:, t] / (Param.G[t+1] * N[:,t+1])
            normZ[:, t + 1] += V[:, t + 1]*w*(Param.tauPAYGO) / (Param.G[t + 1] * N[:, t + 1])
        elif t+1 == Param.TRet:
            normZ[:, t + 1] = ( (1+r) * normZ[:, t]
                + V[:, t+1]*w*(Param.tauPAYGO) )
            normX[:, t + 1] = (1+r) * normS[:, t] + Param.rho * w + normZ[:, t + 1]
            normZ[:, t + 1] = 0
        elif t+1 > Param.TRet:
            normZ[:, t + 1] = 0
            normX[:, t + 1] = (1 + r) * normS[:, t] + Param.rho * w
        else:
            raise BaseException('this should never happen')
        # ensure z_t+1 within z-grid

        normZ[:, t + 1] = np.minimum(normZ[:, t + 1], Param.zGrid.max())

    S = normS * P
    X = normX * P
    Z = normZ * P

    return S, X, Y, Z

def simulate(r, w, policies, Param):
    methods = {
        'PAYGO':simulatePaygo,
        'HYBRID':simulateHybrid,
        'REDISTRIBUTIVE':simulateRedistributive
    }
    return methods[Param.retirementSystem](r, w, policies, Param)

def simulateTransition(r, w, cPolicies, sPolicies, xGrids, S0, X0, Param, Param2):
    """
    Simulates the model
    :param policies: Consumption and Savings Policies
    :param Param: Parameters object
    :return: S, X, Y: De-normalized savings, cash-on-hand, and income for simulated individuals,
    all of shape (I, T)
    """

    # permanent
    N = np.ones((Param.TT, Param.T, Param.I))
    N[:, 0:Param.TRet, :] = random.lognormal(Param.muN, np.sqrt(Param.varN), (Param.TT, Param.TRet, Param.I))

    # transitory
    V = np.ones(N.shape)
    V[:, 0:Param.TRet, :] = random.lognormal(Param.muV, np.sqrt(Param.varV), (Param.TT, Param.TRet, Param.I))

    # permanent income component
    P = np.ones(N.shape)
    P[:, 0, :] = Param.G[0] * N[:, 0, :]
    #for t in np.arange(1, Param.TT):
    for j in np.arange(1, Param.T):
        P[:, j, :] = Param.G[j] * P[:, j - 1, :] * N[:, j, :]

    # not sure if I need this
    P[:, Param.TRet:, :] = P[:, Param.TRet - 1, :].reshape((Param.TT, 1, Param.I))
    #V[:, Param.TRet:, :] = Param.rho

    # todo does this work?
    V[1:, ...] = V[0, ...][np.newaxis, ...]
    P[1:, ...] = P[0, ...][np.newaxis, ...]
    N[1:, ...] = N[0, ...][np.newaxis, ...]

    #Y = P * V * w

    # enforce default negative values to find errors more easily
    normX = np.zeros(N.shape)  -1# evolution of states: normalized cash on hands
    normS = np.zeros(normX.shape) -1  # simulated behavior: normalized savings
    normZ = np.zeros(normX.shape)
    normX[:, 0, :] = V[:, 0, :]
    #normZ[:, 0, :] = 0
    normS[:, -1, :] = 0  # last period savings = 0



    #normS[0, ...] = S0.mean() / P[0, ...].mean()
    #normX[0, ...] = X0.mean() / P[0, ...].mean()

    # policies are referenced to birth.
    # state variables are referenced to age.

    # policies[10, 5] = policy with born at year 10, age 5, hence at year 15
    # S[10, 5] = savings of 5-YO at year 10

    # transition and new steady state

    for t in np.arange(0, Param.TT):
        for j in np.arange(Param.T-1):
            if t - j < 0:
                continue
            if t - j >= Param.TT-Param.T+1:
                #print t, j
                continue


            if t < Param.T:
                #splitAge = Param.T - t
                #if j < splitAge:
                    # old ages are not 2-d interpolatable, as grids dont vary for Z > 0
                interpolateMe = InterpolatedUnivariateSpline(
                    np.hstack((0, xGrids[t, j, 0, :].copy())),
                    np.hstack((0, sPolicies[t, j, 0, :])),
                    k=1)
                normS[t, j, :] = interpolateMe(normX[t, j, :].reshape(-1)).reshape(normX[t, j, :].shape)
                if j + 1 < Param.TRet:
                    normX[t + 1, j + 1, :] = (
                        (1 + r[t]) * normS[t, j, :] / (Param.G[j + 1] * N[t + 1, j + 1, :])
                                              + V[t + 1, j + 1, :] * w[t] * (1 - Param.tauSS))
                elif j + 1 >= Param.TRet:
                    normX[t + 1, j + 1, :] = (1 + r[t]) * normS[t, j, :] + Param.rho * w[t]
                else:
                    raise BaseException('this should never happen')
                if any(np.isnan(normS[t, j, :])):
                    raise BaseException
                if any(normS[t, j, :] < 0):
                    raise BaseException
                continue
            # if t > Param.T, or t + j > Param.T
            interpolateMe = interp2d(xGrids[t, j, ...].T, Param2.zGrid, sPolicies[t, j, ...].T,
                                     extrapolateSecondAxis=True,
                                     xMax = normX[t, j, :].max() * 2 + xGrids[t, j, ...].max())
            normS[t, j, :] = interpolateMe(
                normX[t, j, :].reshape(-1), normZ[t, j, :].reshape(-1)).reshape(normX[t, j, :].shape)

            if any(np.isnan(normS[t, j, :])):
                raise BaseException
            if any(normS[t, j, :] < 0):
                raise BaseException
            # retirement model: G_t+1 == 0
            if j + 1 < Param.TRet:
                normX[t + 1, j + 1, :] = (1 + r[t]) * normS[t, j, :] / (Param2.G[j + 1] * N[t + 1, j + 1, :])
                normX[t + 1, j + 1, :] += V[t + 1, j + 1, :] * w[t] * (1 - Param2.tauSS)

                normZ[t + 1, j + 1, :] = (1 + r[t]) * normZ[t, j, :] / (Param2.G[j + 1] * N[t + 1, j + 1, :])
                normZ[t + 1, j + 1, :] +=  (
                    V[t + 1, j + 1, :] * w[t] * (Param.tauPAYGO) / (Param.G[j + 1] * N[t + 1, j + 1, :]) )
            elif j + 1 == Param.TRet:
                normZ[t + 1, j + 1, :] = ((1 + r[t]) * normZ[t, j, :]
                                   + V[t + 1, j + 1, :] * w[t] * (Param2.tauPAYGO))
                normX[t + 1, j + 1, :] = (1 + r[t]) * normS[t, j, :] + Param2.rho * w[t] + normZ[t + 1, j + 1, :]
                normZ[t + 1, j + 1, :] = 0
            elif j + 1 > Param.TRet:
                normZ[t + 1, j + 1, :] = 0
                normX[t + 1, j + 1, :] = (1 + r[t]) * normS[t, j, :] + Param2.rho * w[t]
            else:
                raise BaseException('this should never happen')

            # some case-catching
            if t > 100 and j + 1 < Param.TRet and any(normZ[t +1 , j + 1, :] == 0):
                raise Exception
            if any(np.isnan(normX[t + 1, j + 1, :])):
                print normX[t + 1, j + 1, :]
            if any(np.isnan(normZ[t + 1, j + 1, :])):
                print normZ[t + 1, j + 1, :]

    S = normS * P
    X = normX * P
    Z = normZ * P

    return S, X, Z

def interp2d(xResult, zGrid, cResult, extrapolateSecondAxis=False, xMax = None, zMax = None):

    if xMax is None:
        xMax = 10000
    if zMax is None:
        zMax = 1000

    if extrapolateSecondAxis == False:
        xx = np.zeros((xResult.shape[0] + 2, xResult.shape[1]))
        xx[1:-1, :] = xResult
        cc = np.zeros(xx.shape)
        cc[1:-1, :] = cResult
        xx[-1, :] = xMax  # "large"
        cc[-1, :] = cc[-2, :] + (cc[-2, :] - cc[-3, :]) / (xx[-2, :] - xx[-3, :]) * (xx[-1, :] - xx[-2, :])
        xx[0, :] = 0
        cc[0, :] = 0
        zz = zGrid + np.zeros(xx.shape)
    else:
        xx = np.zeros((xResult.shape[0] + 2, xResult.shape[1]+2))
        xx[1:-1, 1:-1] = xResult
        cc = np.zeros(xx.shape)
        cc[1:-1, 1:-1] = cResult
        zz = np.zeros(zGrid.shape[0]+2)
        zz[1:-1] = zGrid
        zz[0] = -1000
        zz[-1] = zMax

        xx[-1, :] = xMax + zz # "large", need
        xx[0, :] = 0

        xx[:, 0] = xx[:, 1] + (xx[:, 2] - xx[:, 1]) / (zz[2] - zz[1]) * (zz[1] - zz[0])
        xx[:, -1] = xx[:, -2] + (xx[:, -2] - xx[:, -3]) / (zz[-2] - zz[-3]) * (zz[-1] - zz[-2])


        cc[-1, :] = cc[-2, :] + (cc[-2, :] - cc[-3, :]) / (xx[-2, :] - xx[-3, :]) * (xx[-1, :] - xx[-2, :])
        cc[0, :] = 0

        # [0,0] corner has x=0, c=0. cannot extrapolate
        # if no descent at all, just copy:
        if xResult.var(axis=1).max() < 1e-6 and False:
            cc[:, 0] = cc[:, 1]
            xx[:, 0] = xx[:, 1]
            cc[:, -1] = cc[:, -2]
            xx[:, -1] = xx[:, -2]
        else:
            # if no descent in z-axis, over last 2 entries, take last 3 entries for approximation of fPrime
            isZero = xx[:, 2] - xx[:, 1] == 0
            nZero = isZero == False
            isZero[0] = 0# ignore 0-0 corner with 0 consumption
            nZero[0] = 0
            cc[1:, 0] = cc[1:, 1]
            cc[nZero, 0] += (cc[nZero, 2] - cc[nZero, 1]) / (xx[nZero, 2] - xx[nZero, 1]) * (xx[nZero, 1] - xx[nZero, 0])
            #cc[isZero, 0] += ( (cc[isZero, 3] - cc[isZero, 1]) / (xx[isZero, 3] - xx[isZero, 1])
            #                 * (xx[isZero, 1] - xx[isZero, 0]) )
            cc[isZero, 0] = cc[isZero, 1]

            isZero = xx[:, -2] - xx[:, -3] == False
            nZero = isZero == False
            isZero[0] = 0# ignore 0-0 corner with 0 consumption
            nZero[0] = 0
            cc[1:, -1] = cc[1:, -2]
            cc[nZero, -1] += ( (cc[nZero, -2] - cc[nZero, -3]) / (xx[nZero, -2] - xx[nZero, -3])
                               * (xx[nZero, -1] - xx[nZero, -2]) )
            #cc[isZero, -1] += ( (cc[isZero, -2] - cc[isZero, -4]) / (xx[isZero, -2] - xx[isZero, -4])
            #                  * (xx[isZero, -1] - xx[isZero, -2]) )


        zz = zz + np.zeros(xx.shape)

    return interpolate.LinearNDInterpolator((xx.flatten(), zz.flatten()), cc.flatten(),
                                            rescale=True)

def calibrateCapitalOutputRatio(target, Param):
    """
    Solves for beta : K(beta)/Y(beta) == target
    Note: Changes Param.beta
    :param Param: parameters
    :return: optimal beta
    """
    r = Param.alpha/target - Param.delta
    #K = target**(1/(1-Param.alpha)) * labor
    K, w = GE.getKandW(r, Param)

    def calibrateCapitalOutputRatioError(beta, K, r, w, Param):
        Param.beta = beta
        savings = solveCapitalSupply(r=r, w=w, Param=Param)
        return savings-K

    beta, output = optimize.brentq(calibrateCapitalOutputRatioError, 0.7, 1.3,
                                   args=(K, r, w, Param),
                                   full_output=True)
    # calibrateCapitalOutputRatioError(Param.beta, target, theta, WFunction, method, Param)
    if beta >= 1:
        raise RuntimeWarning('calibrated beta >= 1: ' + str(beta))
    Param.beta = beta
    Param.r = r
    return beta

def movingaverage (values, window):
    weights = np.repeat(1.0, window)/window
    sma = np.convolve(values, weights, 'same')
    sma[:window] = values[:window]
    sma[-window:] = values[-window:]
    return sma

def computeTransition(Param, Param2):
    """
    computes transition from PAYGO (with Parameters Param) to HYBRID (with Parameters Param2)
    :param Param:
    :param Param2:
    :return:
    """

    # first: compute both steady states


    Param.I = Param2.I
    k0, w0 = GE.getKandW(Param.r, Param)
    policies0 = computePolicies(Param.r, w0, Param)
    S0, X0, Y0 = simulate(Param.r, w0, policies0, Param)
    # GE. method does not work with HYBRID, as it returns additional matrix Z

    maxIt = 1000
    rHistory = np.zeros((maxIt, Param.TT))
    netHistory = np.zeros((maxIt, Param.TT))

    # initial interest rate guess: linear
    r = np.zeros((Param.TT,))
    rDot = (Param2.r - Param.r) / (Param.TT - 2 * Param.T)
    r[:Param.T] = Param.r
    #r[Param.T:Param.TT - Param.T] = Param.r + np.arange(Param.TT - 2 * Param.T) * rDot
    #r[Param.TT - Param.T:] = Param2.r
    r[Param.T:] = Param2.r
    rHistory[0, :] = r

    xi = 0.0005 # updating strength

    try:
        #raise IOError('dont case')
        npz = np.load('history.npz')
        rHistoryOld, netHistoryOld = npz['rHistory'], npz['netHistory']
        ixStart = rHistoryOld.argmin(axis=0)[0]


        if ixStart < 1:
            raise IOError('useless file')
        rHistory[:ixStart, :] = rHistoryOld[:ixStart, :]
        netHistory[:ixStart, :] = netHistoryOld[:ixStart, :]
        print 'loaded cached histories'
        # in case we start with empty cache
        if ixStart < 1:
            ixStart = 1

    except IOError:
        ixStart = 1
        rHistory[0, Param.T - 1:Param.TT - Param.T + 1] = Param2.r
        pass


    for i in np.arange(ixStart - 1, maxIt):
        r = np.zeros((Param.TT,))
        r[:Param.T] = Param.r
        r[Param.T - 1:Param.TT - Param.T + 1] = rHistory[i, Param.T - 1:Param.TT - Param.T + 1]
        r[Param.TT - Param.T:] = Param2.r


        method = computePolicies
        sPolicies = np.ones((Param.TT, Param.T, Param2.nZ, Param.nS)) * (-1)
        cPolicies = np.ones((Param.TT, Param.T, Param2.nZ, Param.nS)) * (-1)
        xGrids = np.ones((Param.TT, Param.T, Param2.nZ, Param.nS)) * (-1)
        w, K = np.ones(Param.TT), np.ones(Param.TT)

        # getKandW is not vectorizable
        for t in np.arange(Param.TT):
            if t == 0:
                P = Param
            else:
                P = copy.deepcopy(Param2)
            K[t], w[t] = GE.getKandW(r[t], P)

        # fill convergence
        for t in np.arange(0, Param.TT-Param.T+1):

            if t < Param.T : # need to mix old and new policies
                #splitAge = Param.T - t # age at which new policy starts
                cOld, sOld = method(r[t:t+Param.T], w[t:t+Param.T], Param)
                c, s  = method(r[t:t+Param.T], w[t:t+Param.T], P)

                for j in np.arange(Param.T):

                    if t + j < Param.T:
                        cc, ss, xx = cOld, sOld, Param.xGrid
                        cPolicies[t + j, j, 0, :] = cc[:, j].T
                        sPolicies[t + j, j, 0, :] = ss[:, j].T
                        xGrids[t + j, j, 0, :] = xx[:, j].T
                    else:
                        cc, ss, xx = c, s, P.xGrid
                        cPolicies[t + j, j, ...] = cc[..., j].T
                        sPolicies[t + j, j, ...] = ss[..., j].T
                        xGrids[t + j, j, ...] = xx[..., j].T
                continue

            c, s = method(r[t:t + Param.T], w[t:t + Param.T], P)
            for j in np.arange(Param.T):
                cPolicies[t + j, j, ...] = c[..., j].T
                sPolicies[t + j, j, ...] = s[..., j].T
                xGrids[t + j, j, ...] = P.xGrid[..., j].T

                if False:
                    # right end of grid, wont have policies for "too young" generations
                    for t in np.arange(Param.TT-Param.T+1, Param.TT):
                        c, s = method(r[t:t + Param.T], w[t:t + Param.T], P)
                        cPolicies[t, ...] = c.swapaxes(0, 2)
                        sPolicies[t, ...] = s.swapaxes(0, 2)
                        xGrids[t, ...] = P.xGrid.swapaxes(0, 2)

        # no caching
        if False:
            np.savez('policies.npz', c=cPolicies, s=sPolicies, x=xGrids)
        if False:
            npz = np.load('policies.npz')
            cPolicies, sPolicies, xGrids = npz['c'], npz['s'], npz['x']
        if True:
            S, X, Z = simulateTransition(r, w, cPolicies, sPolicies, xGrids, S0, X0, Param, Param2)
            if False:
                np.savez('temptransition.npz', S=S, X=X, Z=Z)
        # no caching
        if False:
            npz2 = np.load('temptransition.npz')
            S, X, Z = npz2['S'], npz2['X'], npz2['Z']

        SS = S.copy()
        SS[S < 0] = 0

        savings = (SS+Z).sum(axis=-1).sum(axis=-1)/ (Param.T * Param.I)
        # overwrite initial period with steady state
        savings[:Param.T] = S0.mean()
        savings[-Param.T:] = savings[-Param.T-1]

        netCapital = savings - K

        ssDiff = np.diff(netCapital, 1)
        if np.mod(i, 2) == 0:
            dSS = np.hstack((ssDiff, netCapital[-1] - netCapital[-2]))
        else:
            dSS = np.hstack((netCapital[1] - netCapital[0], ssDiff))

        netHistory[i, :] = netCapital

        if i > 50 and np.abs(netHistory[i, ...].max()) < 1:
            print 'done.'
            return None

        if i+1 < maxIt:
            deltaR = xi * dSS
            deltaR = np.maximum(deltaR, -0.02)
            deltaR = np.minimum(deltaR, 0.02)
            rHistory[i+1, :] = rHistory[i, :] - deltaR
            # enforce lower bound -delta + 0.05
            rHistory[i+1, rHistory[i+1, :] < - Param.delta] = - Param.delta + 0.05

        if i < 20:
            rHistory[i+1, Param.T:] = movingaverage(rHistory[i+1, Param.T:], 50)
        elif i<60:
            rHistory[i+1, Param.T:] = movingaverage(rHistory[i+1, Param.T:], 10)
        elif i < 100:
            #following original code, to stabilize end of grid
            rHistory[i+1, Param.T:] = movingaverage(rHistory[i+1,Param.T:], 5)
        elif i == 100:
            xi=1*np.exp(-0.0006*np.arange(0,Param.TT))
        elif i == 150:
            xi=1*np.exp(-0.00006*np.arange(0,Param.TT))


        plt.plot(netCapital)
        plt.savefig('zz' + str(i) + '.pdf')
        plt.close()

        np.savez('history.npz', rHistory=rHistory, netHistory=netHistory)
        gc.collect() # collect garbage (let's hope my code doesn't get removed)
        print i, np.abs(netHistory[i, :]).max()

    return 1

def solveCapitalSupply(r, w, Param):
    """
    solves for a(r), capital supply
    :param r: interest rate
    :param w: wages
    :param theta: death rates
    :param WFunction: bequest function
    :param method: method of solving the problem (solveValueFunction, endogenousGridPointMethod)
    :param Param: parameters
    :return: total capital
    """
    policies = computePolicies(r=r, w=w, Param=Param)

    # i assume measure 1 of households
    if Param.retirementSystem == 'HYBRID':
        S, X, Y, Z = simulate(policies=policies, Param=Param, r=r, w=w)
        totalSavings = S.mean() + Z.mean()
    else:
        S, X, Y = simulate(policies=policies, Param=Param, r=r, w=w)
        totalSavings = S.mean()
    return totalSavings

def solveEquilibrium(Param):
    """
    solves for r: a(r) == k(r). Stores also in Param.r

    :param Param: parameters
    :return: r equilibrium interest rate
    """
    def solveEquilibriumError(r, Param):
        KDemand, w = GE.getKandW(r, Param)
        capitalSupply = solveCapitalSupply(r, w,  Param)
        return capitalSupply -KDemand

    r, results = optimize.brentq(solveEquilibriumError, -Param.delta + 1e-6, 3,
        args=(Param), full_output=True, xtol=1e-6)
    # solveEquilibriumError(2, theta, WFunction, method, Param)
    KDemand, w = GE.getKandW(r, Param)
    capitalSupply = solveCapitalSupply(r, w, Param)
    print np.abs(KDemand/capitalSupply - 1)
    if np.abs(KDemand/capitalSupply - 1) > 0.1:
        print 'Warning: Did not find a good solution for r given beta = ' + str(Param.beta) + \
              ' (error of ' + str(np.abs(KDemand/capitalSupply - 1)) + ' )'

    Param.r = r
    return r

def getStatisticsHybrid(S, X, Y, Z):
    def getGini(x):
        # requires all values in x to be zero or positive numbers,
        # otherwise results are undefined
        n = len(x)
        s = x.sum()
        r = np.argsort(np.argsort(-x))  # calculates zero-based ranks
        return 1 - (2.0 * (r * x).sum() + s) / (n * s)
    def getShares(x, y):
        """
        gets shares in Y, based on percentiles over x
        """
        xxPercentiles = getPercentiles(x)
        shares = np.zeros(len(xxPercentiles))
        for i in np.arange(len(xxPercentiles)):
            if i == 0:
                lower = 0
            else:
                lower = xxPercentiles[i - 1]
            shares[i] = y[(x > lower) & (x <= xxPercentiles[i])].sum()/y.sum()
        return shares
    def getPercentiles(x):
        percentiles = np.arange(20, 120, 20)
        xx = x.reshape((-1)).copy()
        xx.sort()
        xxPercentiles = [np.percentile(xx, p) for p in percentiles]
        return xxPercentiles
    # some of these statistics need to be redone
    statistics = GE.getStatistics(S, X, Y)

     # average wealth by age
    mX = (S+Z).mean(axis=0)

    # gini coefficients for wealth
    gX = getGini((S+Z).reshape((-1)))

    # shares of income, consumption and wealth by quintiles of wealth distribution
    incomeShares = getShares(S+Z, Y)
    consumptionShares = getShares(S+Z, X-S)
    wealthShares = getShares(S+Z, S)

    statistics['mX'] = mX
    statistics['gX'] = gX
    statistics['incomeShares'] = incomeShares
    statistics['consumptionShares'] = consumptionShares
    statistics['wealthShares'] = wealthShares

    return statistics

def getStatisticsTransition(r, S0, X0, Param, Param2):

    method = computePolicies
    sPolicies = np.ones((Param.TT, Param.T, Param2.nZ, Param.nS)) * (-1)
    cPolicies = np.ones((Param.TT, Param.T, Param2.nZ, Param.nS)) * (-1)
    xGrids = np.ones((Param.TT, Param.T, Param2.nZ, Param.nS)) * (-1)
    w, K = np.ones(Param.TT), np.ones(Param.TT)

    # getKandW is not vectorizable
    for t in np.arange(Param.TT):
        if t == 0:
            P = Param
        else:
            P = copy.deepcopy(Param2)
        K[t], w[t] = GE.getKandW(r[t], P)

        # fill convergence
    for t in np.arange(0, Param.TT - Param.T + 1):

        if t < Param.T:  # need to mix old and new policies
            # splitAge = Param.T - t # age at which new policy starts
            cOld, sOld = method(r[t:t + Param.T], w[t:t + Param.T], Param)
            c, s = method(r[t:t + Param.T], w[t:t + Param.T], P)

            for j in np.arange(Param.T):

                if t + j < Param.T:
                    cc, ss, xx = cOld, sOld, Param.xGrid
                    cPolicies[t + j, j, 0, :] = cc[:, j].T
                    sPolicies[t + j, j, 0, :] = ss[:, j].T
                    xGrids[t + j, j, 0, :] = xx[:, j].T
                else:
                    cc, ss, xx = c, s, P.xGrid
                    cPolicies[t + j, j, ...] = cc[..., j].T
                    sPolicies[t + j, j, ...] = ss[..., j].T
                    xGrids[t + j, j, ...] = xx[..., j].T
            continue

        c, s = method(r[t:t + Param.T], w[t:t + Param.T], P)
        for j in np.arange(Param.T):
            cPolicies[t + j, j, ...] = c[..., j].T
            sPolicies[t + j, j, ...] = s[..., j].T
            xGrids[t + j, j, ...] = P.xGrid[..., j].T

    S, X, Z = simulateTransition(r, w, cPolicies, sPolicies, xGrids, S0, X0, Param, Param2)
    S, X, Z = S[Param.T:-Param.T], X[Param.T:-Param.T], Z[Param.T:-Param.T]

    # now: compute average utility at each point for each cohort
    C = X - S
    C[C <= 0] = 1e-6
    UTransition = bequest.Utility(C, Param2)[Param.T:-Param.T] # ignore first T period for transition

    C0 = X0 - S0
    C0[X0 < 0] = 1e-6
    USteadyState = bequest.Utility(C0, Param2)

    print 'Utility Old: ' + str(USteadyState.mean())
    print 'Utility New: ' + str(UTransition.mean(axis=-1).mean(axis=-1).sum()/(Param.TT))

    # todo consumption equivalence

    # now, compare utility for each cohort at each point in time
    newIsBetter = (UTransition - USteadyState.T[np.newaxis, ...]).mean(axis=-1)

    plt.figure()
    plt.plot(newIsBetter)
    plt.savefig('../content/ass3q1transcomp1.pdf')
    plt.close()


    #UOldWeight = np.array([Param.beta] * np.arange(Param.T)) ** np.arange(Param.T)
    oldUtilityAveraged = USteadyState.mean(axis=0)
    for j in np.arange(2, Param.T+1):
        oldUtilityAveraged[-j] += Param.beta * oldUtilityAveraged[-j+1]

    newUtilityAveraged = UTransition.mean(axis=-1).copy()
    for t in np.arange(Param2.T, Param.TT-Param2.T)[::-1]:
        for j in np.arange(2, Param.T+1):
            newUtilityAveraged[t - j, Param.T - j] += Param.beta * newUtilityAveraged[t - j +1, Param.T - j + 1]


    from mpl_toolkits.axes_grid1 import make_axes_locatable
    plt.figure()
    ax = plt.gca()
    im = ax.imshow(newUtilityAveraged[Param.T:-Param.T] - oldUtilityAveraged)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size='5%', pad=0.05)
    plt.colorbar(im, cax=cax)
    plt.savefig('../content/ass3q1transcomp2.pdf')
    plt.close()

    fig, ax1 = plt.subplots()

    ax2 = ax1.twinx()
    ax1.plot(r, label='r')
    ax2.plot(w, 'k', label='w')
    #ax2.plot(K, label='K')
    ax1.legend()
    ax1.set_xlabel('Time')
    #ax1.set_ylabel('Price')
    #ax2.set_ylabel('Quantity')
    ax2.legend(loc=2)
    plt.savefig('../content/ass3q1transprices.pdf', bbox_inches="tight", pad_inches=0)
    plt.close()

    fig, ax = plt.subplots()
    plt.plot(K, label='k')
    plt.legend()
    plt.savefig('../content/ass3q1transk.pdf', bbox_inches="tight", pad_inches=0)
    plt.close()
    print 1

def question1ab():
    ParamBenchmark = Parameters(retirementSystem='PAYGO')
    #
    ParamBenchmark.r, ParamBenchmark.beta = 0.02, 0.96
    #calibrateCapitalOutputRatio(target=3, Param=ParamBenchmark)
    K, w = GE.getKandW(ParamBenchmark.r, ParamBenchmark)
    policies = computePolicies(r=ParamBenchmark.r, w=w, Param=ParamBenchmark)
    S, X, Y = simulate(r=ParamBenchmark.r, w=w, policies=policies, Param=ParamBenchmark)
    statsBenchmark = GE.getStatistics(S, X, Y)

    ParamRedist = Parameters(retirementSystem='REDISTRIBUTIVE')
    ParamRedist.beta = ParamBenchmark.beta
    #solveEquilibrium(ParamRedist)
    ParamRedist.r = 0.0068
    K, w = GE.getKandW(ParamRedist.r, ParamRedist)
    policies = computePolicies(r=ParamRedist.r, w=w, Param=ParamRedist)
    S2, X2, Y2 = simulate(r=ParamRedist.r, w=w, policies=policies, Param=ParamRedist)
    statsRedist = GE.getStatistics(S2, X2, Y2)

    ParamHybrid = Parameters(retirementSystem='HYBRID')
    ParamHybrid.beta = ParamBenchmark.beta
    #solveEquilibrium(ParamHybrid)
    ParamHybrid.r = 0.00455
    K, w = GE.getKandW(ParamHybrid.r, ParamHybrid)
    policies = computePolicies(r=ParamHybrid.r, w=w, Param=ParamHybrid)
    S3, X3, Y3, Z3 = simulate(r=ParamRedist.r, w=w, policies=policies, Param=ParamHybrid)
    statsHybrid = getStatisticsHybrid(S3, X3, Y3, Z3)

    statistics = {'hybrid': statsHybrid, 'bench': statsBenchmark, 'redist':statsRedist}

    print 'Benchmark: Replacement Ratio rho: ' + str(ParamBenchmark.rho)
    print 'Hybrid: Replacement Ratio rho: ' + str(ParamHybrid.rho)

    df = pd.DataFrame(columns=['Replacement Rate', 'Gini (Earnings)', "Gini (Wealth)'"],
                      index=['Benchmark', 'Redistributive', 'Hybrid'])
    df.loc['Benchmark', :] = [ParamBenchmark.rho, statsBenchmark['gY'], statsBenchmark['gX']]
    df.loc['Redistributive', :] = [ParamRedist.rho, statsRedist['gY'], statsRedist['gX']]
    df.loc['Hybrid', :] = [ParamHybrid.rho, statsHybrid['gY'], statsHybrid['gX']]
    df.to_latex('../content/ass3q1stats.tex')

    plt.figure()
    plt.plot(Z3.mean(axis=0), label='Retirement Account')
    plt.plot(S3.mean(axis=0), label='Savings')
    plt.legend(loc=2)
    plt.savefig('../content/ass3q1z.pdf', bbox_inches="tight", pad_inches=0)
    plt.close()

    myList = {'earnings': 'vS', 'consumption': 'vC'}
    for what in myList:
        key = myList[what]
        plt.figure()
        plt.plot(statsBenchmark[key], label='Benchmark')
        plt.plot(statsRedist[key], label='Redistributive')
        plt.plot(statsHybrid[key], label='Hybrid')
        plt.legend(loc=2)
        plt.savefig('../content/ass3q1' + what + '.pdf', bbox_inches="tight", pad_inches=0)
        plt.close()

    N = 5 # 5 percentiles
    ind = np.arange(N)+1  # the x locations for the groups
    width = 0.25  # the width of the bars
    myList = ['incomeShares', 'consumptionShares', 'wealthShares']
    for item in myList:
        fig, ax = plt.subplots()
        rects1 = ax.bar(ind - width, statsBenchmark[item], width, color='#554444', align='center')
        rects2 = ax.bar(ind, statsRedist[item], width, color='#777788', align='center')
        rects3 = ax.bar(ind + width, statsHybrid[item], width, color='#bbccbb', align='center')

        # add some text for labels, title and axes ticks
        ax.set_ylabel('Shares')
        #ax.set_xticks(ind + width)
        ax.set_xticklabels(('0', '0.2', '0.4', '0.6', '0.8', '1'))
        plt.ylim(0, 0.65)
        plt.xlim(0.5, 5.5)

        ax.legend((rects1[0], rects2[0], rects3[0]), ('Benchmark', 'Redistribution', 'Hybrid'), loc=2)
        plt.savefig('../content/ass3q1' + item + '.pdf', bbox_inches="tight", pad_inches=0)
        plt.close()

    for statsName in statistics:
        stats = statistics[statsName]
        if False:
            plt.figure()
            plt.plot(stats['vS'], label='log earnings')
            plt.plot(stats['vC'], label='log consumption')
            plt.legend()
            plt.savefig('../content/ass3q1' + statsName + 'variance.pdf', bbox_inches="tight", pad_inches=0)
            plt.close()
        plt.figure()
        plt.plot(stats['mC'], label='Consumption')
        plt.plot(stats['mX'], label='Wealth')
        plt.plot(stats['mY'], label='Income')
        plt.ylim(0, 300)
        plt.xlim(0, ParamBenchmark.T)
        plt.legend()
        plt.savefig('../content/ass3q1' + statsName + 'mean.pdf', bbox_inches="tight", pad_inches=0)
        plt.close()

def question1c():
    ParamBenchmark = Parameters(retirementSystem='PAYGO')
    #ParamBenchmark.TT = 180
    ParamBenchmark.TT = 240
    calibrateCapitalOutputRatio(target=3, Param=ParamBenchmark)
    ParamHybrid = Parameters(retirementSystem='HYBRID')
    ParamHybrid.beta = ParamBenchmark.beta
    ParamHybrid.I = 1000
    ParamBenchmark.I = 1000
    solveEquilibrium(ParamHybrid)
    if False:
        rHistory = computeTransition(ParamBenchmark, ParamHybrid)
        r = rHistory[rHistory.argmin(ParamHybrid=0)[0] - 1, ...]
    elif False:
        npz = np.load('history.npz')
        rHistory = npz['rHistory']
        npz.close()
        r = rHistory[rHistory.argmin(ParamHybrid=0)[0] - 1, ...]
    else:
        r = np.zeros((ParamBenchmark.TT,))
        rDot = (ParamHybrid.r - ParamBenchmark.r) / (ParamBenchmark.TRet)
        r[:ParamBenchmark.T] = ParamBenchmark.r
        r[ParamBenchmark.T:] = ParamHybrid.r
        r[ParamBenchmark.T:ParamBenchmark.T + ParamBenchmark.TRet] = (ParamBenchmark.r +
                    np.arange(ParamBenchmark.TRet) * rDot )



    k0, w0 = GE.getKandW(ParamBenchmark.r, ParamBenchmark)
    policies0 = computePolicies(ParamBenchmark.r, w0, ParamBenchmark)
    S0, X0, Y0 = simulate(ParamBenchmark.r, w0, policies0, ParamBenchmark)

    getStatisticsTransition(r, S0, X0, ParamBenchmark, ParamHybrid)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='test')
    parser.add_argument('--part', help="question1bench, question1ab, question1c, or all")
    args = parser.parse_args()
    valid = {'question1ab': question1ab, 'question1c':question1c }

    if args.part is None:
        question1c()
        if False:
            for p in valid:
                print 'doing ' + p
                valid[p]()

    if args.part in valid:
        print 'doing ' + args.part
        valid[args.part]()
    else:
        print 'Invalid part'




"""
solves the bequest model
"""

import numpy as np
import matplotlib.pyplot as plt
plt.interactive(False)
from mpl_toolkits.mplot3d import Axes3D
np.seterr(all='raise')
from scipy import optimize

import scipy.interpolate as interpolate
interp1d = interpolate.interp1d
import numpy.polynomial.hermite as hermite
def extrap1d(interpolator):
    xs = interpolator.x
    ys = interpolator.y

    def pointwise(x):
        if x < xs[0]:
            return ys[0]+(x-xs[0])*(ys[1]-ys[0])/(xs[1]-xs[0])
        elif x > xs[-1]:
            return ys[-1]+(x-xs[-1])*(ys[-1]-ys[-2])/(xs[-1]-xs[-2])
        else:
            return interpolator(x)

    def ufunclike(xs):
        return np.array(map(pointwise, np.array(xs)))

    return ufunclike
import numpy.random as random
random.seed(1)
from scipy.interpolate import InterpolatedUnivariateSpline


class Parameters(object):
    '''
    contains the parameters, and creates the grid points
    '''
    def __init__(self):


        # environment
        self.T = np.float64(80)
        self.I = 10000 # number of individuals for simulation

        # preferences
        self.alpha = 0.1
        self.TRet = np.float64(45) #period of retirement
        self.sigma = np.float64(2) # risk averison
        self.rho = np.float64(0.04) # discount rate
        self.beta = 1 / (1 + self.rho)

        # grid points
        self.sMin = 0
        self.sMax = 20
        self.spacing = 1.5
        self.nS = 250
        self.nX = 200
        self.xMin = 0.001
        self.xMax = 30

        # shocks
        self.r = np.float64(0.02) # interest rate
        self.varN = np.float64(0.0106) # variance of permanent shocks
        self.varV = np.float64(0.0738) # -- transitory shocks
        self.nY = np.float64(5) # number of nodes in quadrature of income
        self.probV0 = np.float64(0.0005) # probability of zero income: 0 => explicit
        self.provV0 = 0


        # life cycle profile of income(deterministic)
        self.G = np.array([
            15.5273036489961, 1.13087311215982, 1.05870874450936, 1.05467219182954, 1.05077711505543,
           1.04702205854521, 1.04340562226528, 1.03992646092071, 1.03658328312142, 1.03337485058359,
           1.03029997736546, 1.02735752913683, 1.02454642248183, 1.02186562423414, 1.01931415084419,
           1.01689106777780, 1.01459548894578, 1.01242657616384, 1.01038353864259, 1.00846563250697,
           1.00667216034486, 1.00500247078444, 1.00345595809984, 1.00203206184495, 1.00073026651485,
           0.999550101234814, 0.998491139476392, 0.997552998800479, 0.996735340627130, 0.996037870031881,
           0.995460335568447, 0.995002529117637, 0.994664285762371, 0.994445483688613, 0.994346044112305,
           0.994365931232058, 0.994505152207672, 0.994763757164433, 0.995141839223198, 0.995639534556274,
           0.996257022469168, 0.996994525508269, 0.997852309594495, 0.998830684183142, 0.999930002449943,
           0.682120000000000, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                  dtype=np.float64)
        # add 15 more ones (age of 85 versus 100)
        self.G = np.hstack((self.G, np.ones((15,))))

        # conditional probability of death
        self.D = np.array([
            0, 0.001456, 0.001536, 0.001554, 0.001526, 0.001480, 0.001443, 0.001416, 0.001408, 0.001418
            , 0.001437, 0.001460, 0.001500, 0.001535, 0.001589, 0.001653, 0.001737, 0.001851, 0.002001
            , 0.002183, 0.002381, 0.002592, 0.002827, 0.003087, 0.003369, 0.003662, 0.003970, 0.004309
            , 0.004694, 0.005125, 0.005602, 0.006107, 0.006617, 0.007104, 0.007570, 0.008042, 0.008550
            , 0.009114, 0.009781, 0.010582, 0.011543, 0.012632, 0.013798, 0.014946, 0.016067, 0.017272
            , 0.018518, 0.019974, 0.021630, 0.023559, 0.025737, 0.028223, 0.031103, 0.034372, 0.037995
            , 0.042023, 0.046338, 0.051072, 0.056262, 0.061944, 0.068159, 0.074947, 0.082352, 0.090417
            , 0.099186, 0.108704, 0.119015, 0.130161, 0.142182, 0.155116, 0.168995, 0.183844, 0.199686
            , 0.216530, 0.234379, 0.253223, 0.273043, 0.293803, 0.315457, 0.337943, 1])

        # mean consumption age 25-84
        self.dataMoments = np.array([
            24.0000, 24.1392, 24.8688, 25.2408, 25.5636, 25.8708, 26.1480, 26.3952, 26.5932,
             26.7636, 26.9736, 27.2352, 27.5340, 27.8328, 28.1160, 28.3944, 28.6788, 28.9632,
             29.2284, 29.4588, 29.6808, 29.8944, 30.0696, 30.1872, 30.2664, 30.3144, 30.3156,
             30.2556, 30.1476, 30.0120, 29.8392, 29.6160, 29.3304, 29.0100, 28.6788, 28.3224,
             27.9276, 27.5124, 27.1236, 26.7588, 26.3952, 26.0364, 25.6896, 25.3680, 25.0476,
             24.7056, 24.3564, 24.0240, 23.7228, 23.4276, 23.1144, 22.7892, 22.4388, 22.0812,
             21.7200, 21.3384, 20.9340, 20.5368, 20.2116])

        self.setGrids()


    def setGrids(self):
        '''
        creates grids, and stores them in the object.
        :return: void
        '''
        # savings grid
        self.sGrid = np.zeros((self.nS,))
        self.sGrid[0] = self.sMin
        for i in range(1, self.nS):
            self.sGrid[i] = self.sGrid[i-1] + (self.sMax - self.sGrid[i-1])/((self.nS - i)**self.spacing)

        # cash on hand grid
        self.xGrid = np.zeros((self.nX))
        self.xGrid[0] = self.xMin
        for i in range(1, self.nX):
            self.xGrid[i] = self.xGrid[i - 1] + (self.xMax - self.xGrid[i-1]) / ((self.nX - i) ** self.spacing)

        # shock grids
        self.muN = -self.varN*0.5
        self.muV = np.log(float(1)/(1 - self.probV0)) - self.varV * 0.5
        nodes, weights = hermite.hermgauss(self.nY)
        self.nGrid = np.exp(np.sqrt(2 * self.varN) * nodes + self.muN)
        self.vGrid = np.exp(np.sqrt(2 * self.varV) * nodes + self.muV)
        self.pN = weights / np.pi ** 0.5
        self.pV = weights / np.pi ** 0.5
        self.nN, self.nV = self.nY, self.nY
        if self.probV0 > 0:
            self.nV += 1
            self.vGrid = np.hstack((0, self.vGrid))
            self.pV = np.hstack([self.probV0, (1-self.probV0) * self.pV])

        # create meshgrids, to avoid loops
        self.PV, self.PN = np.meshgrid(self.pV, self.pN, indexing='ij')
        self.V, self.N = np.meshgrid(self.vGrid, self.nGrid, indexing='ij')
        self.X, self.S, = np.meshgrid(self.xGrid, self.sGrid, indexing='ij')

### utility and bequest functions and their derivatives follow
def Utility(c, Param):
    if Param.sigma == 1:
        return np.log(c)
    else:
        return c**(1-Param.sigma)/(1-Param.sigma)
def fPrime(c, Param):
    # to prevent underflow
    if True:
        try:
            c[c < 1e-100] = 1e-100
        except TypeError:
            if c < 1e-100:
                return 1e-100
    return c**(-Param.sigma)

def cOfFPrime(f, Param):
    return f**(-1/Param.sigma)

def WBequest(A, Param):
    return Param.alpha * A**(1-Param.sigma)/(1-Param.sigma)

def WBequestPrime(A, Param):
    return Param.alpha * A**(-Param.sigma)

def WNoBequest(A, Param):
    return np.zeros(A.shape)

def WNoBequestPrime(A, Param):
    return np.zeros(A.shape)

def solveValueFunction(theta, WFunction, Param, r = None, w = None):
    """
    solves the value function
    :param theta: T-size vector, float, likelihood of survival
    :param WFunction: Bequest function
    :param Param: Parameters object
    :param r: Optional, interest rate. If none provided, will fetch from Param object
    :param w: Optional, wage. If none provided, one is assumed
    :return: S, W: savings policy and corresponding values of the bellman equation
    """

    if r == None:
        r = Param.r
    if w == None:
        w = 1

    # set up the grids, pre-solve utility and bequest utility
    X, S = Param.X, Param.S
    C = X - S
    iValid = C > 0
    cValid = iValid * C + (1-iValid)*1

    uMinimum = Utility(np.min(Param.xGrid), Param)
    U = iValid * Utility(cValid, Param) + (1 - iValid) * (uMinimum - 1000)

    # this is new: bequest utility
    assets = Param.sGrid * (1 + r)
    assets[0] = 1e-64
    bequestUtility = WFunction(assets, Param)[np.newaxis, ...]
    bequestUtility[0, 0] = uMinimum + 500  # strong negative, but better than c=0

    WResult = np.zeros((Param.nX, int(Param.T))) # value function: now contains
    SResult = np.zeros((Param.nX, int(Param.T))) # savings policy

    # this is new: savings at last period not necessarily 0.
    if WFunction == WNoBequest:
        gamma = 1
    else:
        chi = Param.beta * (1 + r) * Param.alpha
        gamma = chi**(-float(1)/Param.sigma)/(1+chi**(-float(1)/Param.sigma))
        # the following should never happen. Good to be safe though
        if gamma > 1:
            raise Exception('gamma too large')
        if gamma < 0:
            raise Exception('gamme too small')
    SResult[:,-1] = Param.xGrid*(1-gamma)
    WResult[:, -1] = Utility(Param.xGrid*gamma, Param)

    # code is structed the same as in the Matlab original
    for t in np.arange(0, Param.T-1, dtype=int)[::-1]:

        # interpolator over W(x_{t+1})
        interpolateMe = InterpolatedUnivariateSpline(
            Param.xGrid, WResult[:, t + 1], k=1)

        if t+1 < Param.TRet:
            # compute expected return by iterating through shocks, but having a S grid
            wPrime = np.zeros((Param.nS))
            for i in np.arange(0, Param.nN):
                for j in np.arange(0, Param.nV):
                    xPrime = Param.sGrid * (1+r)/(Param.G[t+1] * Param.nGrid[i]) + Param.vGrid[j]*w
                    wTemp = interpolateMe(xPrime.reshape(-1)).reshape(xPrime.shape)
                    wPrime += Param.pN[i]*Param.pV[j]*Param.G[t+1]*Param.nGrid[i]**(1-Param.sigma) * wTemp

        else: # no uncertainty
            xPrime = Param.sGrid * (1+r)/Param.G[t+1] + 1.*w
            wPrime = Param.G[t+1]**(1-Param.sigma) * interpolateMe(xPrime)


        # this is new: add theta and bequest motive in W'
        wMatrix = U + Param.beta* ( (1-theta[t])*bequestUtility + theta[t]* wPrime)
        SResult[:, t] = Param.sGrid[wMatrix.argmax(axis=-1)]
        WResult[:, t] = wMatrix.max(axis=-1)

    return SResult, WResult

def solvePolicyFunction(theta, WFunctionPrime, Param, r=None, w = None):
    """
    to be compatible with my earlier code, I have to write this stub. If called, will only raise Exception
    """
    raise NotImplementedError

def solveEndogenousGridPoints(theta, WFunctionPrime, Param, r=None, w = None):
    """

    :param theta: T-size vector, float, likelihood of survival
    :param WFunction: Bequest function
    :param Param: Parameters object
    :param r: Optional, interest rate. If none provided, will fetch from Param object
    :return: C,X: objects of shape (nS, nT) that contain optimal consumption for corresponding x grid
    """


    if r == None:
        r = Param.r
    if w == None:
        w = 1.
    sGrid = Param.sGrid.copy()
    # if p0 > 0, zero savings are never optimal.
    # Endogenous grid points only works for s which is sometimes optimal
    if Param.probV0 > 0:
        sGrid = sGrid[1:]
    nS = len(sGrid)


    cResult = np.zeros((nS, Param.T))
    xResult = np.zeros((nS, Param.T))

    # Last period: solve optimal savings share gamma (see notes)
    xResult[:, -1] = np.linspace(0.01, 10, nS) # arbitrary grid
    if WFunctionPrime == WNoBequestPrime:
        gamma = 1
    else:
        chi = Param.beta * (1 + r) * Param.alpha
        gamma = chi**(-float(1)/Param.sigma)/(1+chi**(-float(1)/Param.sigma))
        # the following should never happen. Good to be safe though
        if gamma > 1:
            raise Exception('gamma too large')
        if gamma < 0:
            raise Exception('gamme too small')
    cResult[:, -1] = xResult[:, -1]*(gamma)


    # iterate over t. Structured similar as matlab original
    for t in np.arange(0, Param.T-1, dtype=int)[::-1]:

        # interpolator over c_t+1(x_t+1)
        interpolateMe = InterpolatedUnivariateSpline(
            np.hstack((0,xResult[:, t + 1])), np.hstack((0,cResult[:, t + 1])), k=1)

        if t+1 < Param.TRet:
            gothicV = np.zeros((nS))
            for i in np.arange(0, Param.nN):
                for j in np.arange(0, Param.nV):
                    xPrime = sGrid * (1+r)/(Param.G[t+1]*Param.nGrid[i]) + Param.vGrid[j] * w
                    cPrime = interpolateMe(xPrime)
                    gothicV += ( Param.beta * (1+r) * Param.pN[i] * Param.pV[j] *
                        ( Param.G[t+1] * Param.nGrid[i])**(-Param.sigma) * fPrime(cPrime, Param))
        else:
            xPrime = sGrid * (1+r)/(Param.G[t+1]) + 1.*w
            cPrime = interpolateMe(xPrime)
            gothicV = Param.beta * (1+r) * (Param.G[t+1])**(-Param.sigma) * fPrime(cPrime, Param)

        # this is new: adjust u'(c_t) for bequest and death
        realGothicV = ( theta[t]*gothicV
                        + (1-theta[t])* Param.beta*(1+r) *WFunctionPrime(sGrid*(1+r),Param) )
        cResult[:,t] = cOfFPrime(realGothicV, Param)
        xResult[:,t] = cResult[:,t] + sGrid
    return cResult, xResult

def solve(method, theta, WFunction, Param, r=None, w = None):
    """
    Runs the method on theta, WFunction, using Param, and unifies output
    :param method: The method to be run: PolicyFunction or Endogenous Grid Point
    :param theta: Survival likelihood
    :param WFunction: the bequest function
    :param Param: Parameters object. Endogenous GP method will store individual xGrid here
    :param r: Optional: Interest rate
    :param w: Optional: wage
    :return: C, S: Consumption and savings policies given xGrid and Time
    """
    result = method(theta, WFunction, Param, r=r, w=w)

    if method == solveEndogenousGridPoints or method.__name__ == 'solveEndogenousGridPoints':
        c, x = result
        s = x - c
        Param.xGrid = x
    elif method == solvePolicyFunction:
        c = result
        s = Param.xGrid[..., np.newaxis] - c
    elif method == solveValueFunction:
        s, w = result
        c = Param.xGrid[..., np.newaxis] - s
    else:
        raise BaseException('invalid method')
    return c, s

def simulate(policies, Param, r = None, w = None):
    """
    Simulates the model
    :param policies: Consumption and Savings Policies
    :param Param: Parameters object
    :return: S, X, Y: De-normalized savings, cash-on-hand, and income for simulated individuals,
    all of shape (I, T)
    """

    if w == None:
        w = 1
    if r == None:
        r = Param.r

    c,s = policies
    # permanent
    N = np.ones((Param.I, Param.T))
    N[:,0:Param.TRet] = random.lognormal(Param.muN, np.sqrt(Param.varN), (Param.I, Param.TRet ) )

    #if Param.retirementSystem != None:
    #    N[:,0:Param.TRet] = random.lognormal(Param.muN, np.sqrt(Param.varN), (Param.I, Param.TRet ) )



    # transitory
    V = np.ones((Param.I, Param.T))
    V[:, 0:Param.TRet] = random.lognormal(Param.muV, np.sqrt(Param.varV), (Param.I, Param.TRet))
    if Param.probV0 > 0:
        incZero = random.uniform(0, 1, size=(Param.TRet))
        V[:, :Param.TRet] *= (incZero > Param.probV0).astype(float)

    # permanent income component
    P = np.ones((Param.I, Param.T))
    P[:,0] = Param.G[0] * N[:,0]
    for t in np.arange(1,Param.T):
        P[:,t] = Param.G[t] * P[:,t-1] * N[:,t]
    # not sure if I need this
    if Param.retirementSystem != None:
        P[:, Param.TRet:] = P[:, Param.TRet-1].reshape((-1,1))
        V[:, Param.TRet:] = Param.rho


    Y = P*V*w

    normX = np.zeros((Param.I, Param.T) ) # evolution of states: normalized cash on hands
    normS = np.zeros((Param.I, Param.T)) # simulated behavior: normalized savings

    normX[:,0] = V[:,0]

    for t in np.arange(0,Param.T-1):
        if Param.xGrid.ndim == 2:
            interpolateMe = InterpolatedUnivariateSpline(
                np.hstack((0, Param.xGrid[:, t])),
                np.hstack((0, s[:, t])),
                k=1)
        else:
            interpolateMe = InterpolatedUnivariateSpline(
                np.hstack((0, Param.xGrid)),
                np.hstack((0, s[:, t])),
                k=1)

        normS[:,t] = interpolateMe(normX[:,t].reshape(-1)).reshape(normX[:,t].shape)
        # retirement model: G_t+1 == 0
        if Param.G[t+1] > 0:
            if Param.retirementSystem != None:
                normX[:,t+1] = (1+r) * normS[:,t] * (Param.G[t+1] * N[:,t+1])**(-1) + V[:,t+1]*w*(1-Param.tauSS)
            else:
                normX[:,t+1] = (1+r) * normS[:,t] * (Param.G[t+1] * N[:,t+1])**(-1) + V[:,t+1]*w
        else:
            if Param.retirementSystem == 'PAYGO':
                normX[:,t+1] = (1+r) * normS[:,t] + Param.rho * w
            else:
                raise NotImplementedError

    S = normS * P
    X = normX * P

    return S, X, Y

def questions1and2():
    """
    creates the figures for questions 1, 2
    :return: set of figures and figure names
    """

    # I need one set of parameters for the value function, but
    # for each grid-point iteration its own set of parameters, as each one has its own
    # cash-on-hand grid

    ParamValue = Parameters()
    ParamEndogND = Parameters()
    ParamEndogD = Parameters()
    ParamEndogDB = Parameters()

    # survival likelihoods
    thetaNoDeath = np.hstack((np.ones((ParamValue.T-1)), 0))
    thetaDeath = 1-ParamValue.D

    # solve optimal policies with bellman equation:
    # once without death, once with death but no bequest, once with death and bequest
    policiesND = solve(solveValueFunction, thetaNoDeath, WNoBequest, ParamValue)
    policiesD = solve(solveValueFunction, thetaDeath, WNoBequest, ParamValue)
    policiesDB = solve(solveValueFunction, thetaDeath, WBequest, ParamValue)

    # same as above for endog. grid point
    policiesEndogenousND = solve(solveEndogenousGridPoints, thetaNoDeath, WNoBequestPrime, ParamEndogND)
    policiesEndogenousD = solve(solveEndogenousGridPoints, thetaDeath, WNoBequestPrime, ParamEndogD)
    policiesEndogenousDB = solve(solveEndogenousGridPoints, thetaDeath, WBequestPrime, ParamEndogDB)

    # simulate all 6 of the above economies
    SND, XND, YND = simulate(policiesND,  ParamValue)
    SD, XD, YD = simulate(policiesD,  ParamValue)
    SDB, XDB, YDB = simulate(policiesDB,  ParamValue)

    SEND, XEND, YEND = simulate(policiesEndogenousND,  ParamEndogND)
    SED, XED, YED = simulate(policiesEndogenousD,  ParamEndogD)
    SEDB, XEDB, YEDB = simulate(policiesEndogenousDB,  ParamEndogDB)


    # plot: benchmark economy (no death)
    fig0 = plt.figure()
    plt.plot(SND.mean(axis=0), label='median S')
    plt.plot((XND - SND).mean(axis=0), label='median C')
    plt.plot(YND.mean(axis=0), label='median Y')
    plt.title('No Death')
    plt.legend()

    # plot: mean savings with EGP (all three economies)
    fig1 = plt.figure()
    plt.plot(SEND.mean(axis=0), label='No Death')
    plt.plot(SED.mean(axis=0), label='Death, No Bequest')
    plt.plot(SEDB.mean(axis=0), label='Death, Bequest')
    ax = plt.gca()
    plt.title('Endogenous Grid Point Method, Mean Savings')
    plt.legend()

    # plot: mean savings with VI (all three economies)
    fig2a = plt.figure()
    plt.title('Value Function Iteration, Mean Savings')
    plt.plot(SND.mean(axis=0), label='No Death')
    plt.plot(SD.mean(axis=0), label='Death, No Bequest')
    plt.plot(SDB.mean(axis=0), label='Death, Bequest')
    ax = plt.gca()
    plt.legend()

    # plot: mean consumption  with VI (all three economies)
    fig2b = plt.figure()
    plt.title('Value Function Iteration, Mean Consumption')
    plt.plot((XND-SND).mean(axis=0), label='No Death')
    plt.plot((XD-SD).mean(axis=0), label='Death, No Bequest')
    plt.plot((XDB-SDB).mean(axis=0), label='Death, Bequest')
    ax = plt.gca()
    plt.legend()



    # plot: consumption  policy for age 60 (all three economies), VI
    fig3a = plt.figure()
    plt.title('Value Function Iteration, Consumption Policy, age = 60')
    plt.plot(ParamValue.xGrid, policiesND[0][:, 40], label='no death')
    plt.plot(ParamValue.xGrid, policiesD[0][:, 40], label='death')
    plt.plot(ParamValue.xGrid, policiesDB[0][:, 40], label='death and bequest')
    ax = plt.gca()
    plt.legend()
    plt.axis([0, 3, 0, 3])


    # plot: savings policy for age 60 (all three economies), VI
    fig3b = plt.figure()
    plt.title('Value Function Iteration, Savings Policy, age = 60')
    plt.plot(ParamValue.xGrid, policiesND[1][:, 40], label='no death')
    plt.plot(ParamValue.xGrid, policiesD[1][:, 40], label='death')
    plt.plot(ParamValue.xGrid, policiesDB[1][:, 40], label='death and bequest')
    ax = plt.gca()
    plt.legend()
    plt.axis([0, 3, 0, 5])

    # plot: consumption policy for age 60 (all three economies), EGP
    fig4a = plt.figure()
    plt.title('Endogenous Grid Point Method, Consumption Policy, Age = 65')
    plt.plot(ParamEndogND.xGrid[:, 40], policiesEndogenousND[0][:, 40],
                           label='no death')
    plt.plot(ParamEndogD.xGrid[:, 40], policiesEndogenousD[0][:, 40],
                            label='death')
    plt.plot(ParamEndogDB.xGrid[:, 40], policiesEndogenousDB[0][:, 40],
                            label='death and bequest')
    plt.xlabel('x')
    plt.legend()
    plt.axis([0, 3, 0, 3])

    # plot: savings policy for a specific cash-on-hand (1.5)
    # to do that, we need to find the index in the coh grid for each T that contains
    # closed value to 1.5
    # for all three economies:
    xTarget = 1.5
    keyND = abs(ParamEndogND.xGrid - xTarget).argmin(axis=0)
    keyD = abs(ParamEndogD.xGrid - xTarget).argmin(axis=0)
    keyDB = abs(ParamEndogDB.xGrid - xTarget).argmin(axis=0)

    fig4b = plt.figure()
    plt.title('Endogenous Grid Point Method, Savings Policy given Cash on Hand')
    plt.plot(np.arange(20, 100), policiesEndogenousND[1][keyND, np.arange(0, int(ParamEndogND.T))],
                           label='no death')
    plt.plot(np.arange(20, 100), policiesEndogenousD[1][keyD, np.arange(0, int(ParamEndogD.T))],
                            label='death')
    plt.plot(np.arange(20, 100), policiesEndogenousDB[1][keyDB, np.arange(0, int(ParamEndogDB.T))],
                            label='death and bequest')
    plt.xlabel('Age')
    plt.legend()

    return {'fig0':fig0, 'fig1':fig1, 'fig2a':fig2a, 'fig2b':fig2b,
            'fig3a':fig3a, 'fig3b':fig3b, 'fig4a':fig4a, 'fig4b':fig4b}

def question3():
    """
    does the estimations. Prints the estimated parameters
    :return: estimated beta, sigma
    """

    def estimate(betaGrid, sigmaGrid):
        """
        the residual function that helps us estimate. Computes residual for a grid of beta, sigma
        :param betaGrid: grid of beta values
        :param sigmaGrid: grid of sigma values
        :return: grid of residuals
        """
        BETA, SIGMA = np.meshgrid(betaGrid, sigmaGrid, indexing='ij')
        RESIDUAL = np.ones(BETA.shape)
        x = 0
        # iterate through the grid
        for idx in np.ndindex(BETA.shape):
            x += 1;
            if np.mod(x, 50) == 0:
                print x, RESIDUAL.size

            # set up beta, sigma from grid as parameters
            Param = Parameters()
            Param.beta, Param.sigma = BETA[idx], SIGMA[idx]

            # solve for policies, simulate
            policies = solve(solveEndogenousGridPoints, thetaDeath, WNoBequestPrime, Param)
            S, X, Y = simulate(policies, Param)

            # compute the differents for ages 25-84
            # in our matrix (starting age 21), this is 5th element onwards
            C = (X - S)[:, 5:numberOfMoments+5]

            # compute residual
            g = (Param.dataMoments - C).mean(axis=0)
            # update result matrix with sum of squared residual
            RESIDUAL[idx] = np.dot(g, np.dot(np.eye(numberOfMoments), g))
        return RESIDUAL


    # we do this whole exercise using endogenous grid points only
    Param = Parameters()
    thetaDeath = 1 - Param.D
    numberOfMoments = len(Param.dataMoments)

    # set up a grid for beta, sigma
    betaMin = 0.3
    betaMax = 0.999
    sigmaMin = 0.1
    sigmaMax = 5
    betaGrid = np.linspace(betaMin, betaMax, 10)
    sigmaGrid = np.linspace(sigmaMin, sigmaMax, 10)
    # compute the residual
    RESIDUAL = estimate(betaGrid, sigmaGrid)

    #now, refine grid around best points
    argMinB = RESIDUAL.min(axis=1).argmin()
    argMinS = RESIDUAL.min(axis=0).argmin()
    # define new grids
    betaGrid = np.linspace(betaGrid[argMinB-1], betaGrid[argMinB+1], 20)
    sigmaGrid = np.linspace(sigmaGrid[argMinS - 1], sigmaGrid[argMinS + 1], 20)

    # do the above step again
    RESIDUAL = estimate(betaGrid, sigmaGrid)
    argMinB = RESIDUAL.min(axis=1).argmin()
    argMinS = RESIDUAL.min(axis=0).argmin()
    # print and return minimizing parameters
    print betaGrid[argMinB], sigmaGrid[argMinS], RESIDUAL[argMinB, argMinS]
    return  betaGrid[argMinB], sigmaGrid[argMinS]

def question4(beta, sigma):
    """
    Does estimations for question 4. Prints parameters, returns the figure
    :param beta: minimizing beta (from q3)
    :param sigma: minimizing sigma (from q3)
    :return: figure of savings-profiles for different parameters
    """

    def estimate(betaGrid, sigmaGrid):
        """
        exactly the same function as in q3. See above for documentation
        """
        BETA, SIGMA = np.meshgrid(betaGrid, sigmaGrid, indexing='ij')
        RESIDUAL = np.ones(BETA.shape)
        x = 0
        for idx in np.ndindex(BETA.shape):
            x += 1;
            if np.mod(x, 50) == 0:
                print x, RESIDUAL.size
            Param = Parameters()
            Param.beta, Param.sigma = BETA[idx], SIGMA[idx]
            policies = solve(solveEndogenousGridPoints, thetaDeath, WNoBequestPrime, Param)
            S, X, Y = simulate(policies, Param)
            C = (X - S)[:, 5:numberOfMoments + 5]
            g = (Param.dataMoments - C).mean(axis=0)
            RESIDUAL[idx] = np.dot(g, np.dot(np.eye(numberOfMoments), g))
        return RESIDUAL

    # take optimal guesses from last exercise, and vary them "slightly"
    betaHigh = beta*1.05
    betaLow = beta*0.95
    sigmaHigh = sigma*1.2
    sigmaLow = sigma*0.8

    # define grids over optimization (around previous solution)
    betaGrid = np.linspace(betaLow, betaHigh, 10)
    sigmaGrid = np.linspace(sigmaLow, sigmaHigh, 10)

    Param = Parameters()
    thetaDeath = 1 - Param.D
    numberOfMoments = len(Param.dataMoments)

    # estimate sigma for betaLow, betaHigh
    RESIDUAL = estimate(np.array([betaLow]), sigmaGrid)
    sigmaBetaLow = sigmaGrid[RESIDUAL.argmin()]
    RESIDUAL = estimate(np.array([betaHigh]), sigmaGrid)
    sigmaBetaHigh = sigmaGrid[RESIDUAL.argmin()]

    # estimate beta for sigmaLow, sigmahigh
    RESIDUAL = estimate(betaGrid, np.array([sigmaLow]))
    betaSigmaLow = betaGrid[RESIDUAL.argmin()]
    RESIDUAL = estimate(betaGrid, np.array([sigmaHigh]))
    betaSigmaHigh = betaGrid[RESIDUAL.argmin()]

    estimations = {'betaLow': {'beta': betaLow, 'sigma':sigmaBetaLow},
                   'betaHigh': {'beta':betaHigh, 'sigma': sigmaBetaHigh},
                   'sigmaLow': {'beta':betaSigmaLow, 'sigma': sigmaLow},
                   'sigmaHigh': {'beta': betaSigmaHigh, 'sigma': sigmaHigh},
                   }
    print estimations

    # for each parameter combination: plot life-cycle savings
    fig = plt.figure()
    for key in estimations.iterkeys():
        Param = Parameters()
        Param.beta = estimations[key]['beta']
        Param.sigma = estimations[key]['sigma']

        policies = solve(solveEndogenousGridPoints, thetaDeath, WNoBequestPrime, Param)
        S, X, Y = simulate(policies, Param)
        plt.title('Endogenous Grid Points method, Savings')
        plt.plot(S.mean(axis=0), label=key)
    plt.title('Savings')
    plt.legend()
    return fig

def question5(beta, sigma):
    """
    does estimations for question 5
    :param beta: optimal beta from question 3
    :param sigma: optimal sigma from question 3
    :return: figure: Variations in beta, sigma, as we change alpha
    """
    def estimate(betaGrid, sigmaGrid, alphaGrid):
        """
        exactly the same as before, but takes in 3 grids instead of two
        """
        BETA, SIGMA, ALPHA = np.meshgrid(betaGrid, sigmaGrid, alphaGrid, indexing='ij')
        RESIDUAL = np.ones(BETA.shape)
        x = 0
        for idx in np.ndindex(BETA.shape):
            x += 1;
            if np.mod(x, RESIDUAL.size/10) == 0:
                print x, RESIDUAL.size
            Param = Parameters()
            Param.beta, Param.sigma, Param.alpha = BETA[idx], SIGMA[idx], ALPHA[idx]
            policies = solve(solveEndogenousGridPoints, thetaDeath, WBequestPrime, Param)
            S, X, Y = simulate(policies, Param)
            C = (X - S)[:, 5:numberOfMoments + 5]
            g = (Param.dataMoments - C).mean(axis=0)
            RESIDUAL[idx] = np.dot(g, np.dot(np.eye(numberOfMoments), g))
        return RESIDUAL

    Param = Parameters()
    thetaDeath = 1 - Param.D
    numberOfMoments = len(Param.dataMoments)

    # create grids
    betaMin = 0.94
    betaMax = 0.98
    sigmaMin = 0.8
    sigmaMax = 4

    betaGrid = np.linspace(betaMin, betaMax, 15)
    sigmaGrid = np.linspace(sigmaMin, sigmaMax, 15)
    alphaGrid = np.linspace(0.05, 5, 10)
    RESIDUAL = estimate(betaGrid, sigmaGrid, alphaGrid)

    # first: given alpha, maximize over the others
    minB = RESIDUAL.min(axis=1).argmin(axis=0)
    minS = RESIDUAL.min(axis=0).argmin(axis=0)

    # plot: optimal beta, sigma, as we vary alpha
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    ax1.plot(alphaGrid, betaGrid[minB], '-r',    label='beta')
    ax2.plot(alphaGrid, sigmaGrid[minS], label='sigma')
    ax2.legend()
    ax1.legend(loc=2)

    # now: solve all three together
    # three grids
    betaGrid = np.linspace(betaMin, betaMax, 12)
    sigmaGrid = np.linspace(sigmaMin, sigmaMax, 12)
    alphaGrid = np.linspace(0.05, 5, 12)


    # compute over all three grids and minimize
    RESIDUAL = estimate(betaGrid, sigmaGrid, alphaGrid)
    minB = RESIDUAL.min(axis=-1).min(axis=-1).argmin()
    minS = RESIDUAL.min(axis=-1).min(axis=0).argmin()
    minA = RESIDUAL.min(axis=0).min(axis=0).argmin()

    # same as in question 3: check around optimal point
    betaGrid = np.linspace(betaGrid[minB - 1], betaGrid[minB + 1], 20)
    sigmaGrid = np.linspace(sigmaGrid[minS - 1], sigmaGrid[minS + 1], 20)
    alphaGrid = np.linspace(alphaGrid[minA - 1], alphaGrid[minA + 1], 20)
    RESIDUAL = estimate(betaGrid, sigmaGrid, alphaGrid)
    minB = RESIDUAL.min(axis=-1).min(axis=-1).argmin()
    minS = RESIDUAL.min(axis=-1).min(axis=0).argmin()
    minA = RESIDUAL.min(axis=0).min(axis=0).argmin()

    # print estimations, return figure
    print 'beta: ' + str(betaGrid[minB])
    print 'sigma: ' + str(sigmaGrid[minS])
    print 'alpha: ' + str(alphaGrid[minA])
    print 'residual '+ str(RESIDUAL.min())
    return fig


if __name__ == "__main__":

        figures = questions1and2()

        for name, figure in figures.iteritems():
            figure.savefig('ass1_' + name + '.pdf')
            plt.close(figure)


        ## question 3: get beta, sigma estimates
        print 'Question 3'
        beta, sigma = question3()

        print 'Question 4'
        fig = question4(beta, sigma)
        fig.savefig('ass1_question4.pdf')
        print 'Question 5'
        fig = question5(beta, sigma)
        fig.savefig('ass1_question5.pdf')





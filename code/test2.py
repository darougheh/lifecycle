
class SuperAbstract(object):
    def __init__(self):
        print 1

class Abstract(SuperAbstract):
    def __init__(self):
        super(self.__class__, self).__init__()
        print 2

class Concrete(Abstract):
    def __init__(self):
        super(self.__class__, self).__init__()
        print 3

foo = Concrete()

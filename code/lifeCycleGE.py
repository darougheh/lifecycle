"""
does GE computations
"""

import lifeCycleBequest as bequest
import numpy as np
import scipy.optimize as optimize
import matplotlib.pyplot as plt
plt.interactive(False)

import copy
from scipy.interpolate import InterpolatedUnivariateSpline

def getStatistics(S, X, Y):
    def getGini(x):
        # requires all values in x to be zero or positive numbers,
        # otherwise results are undefined
        n = len(x)
        s = x.sum()
        r = np.argsort(np.argsort(-x))  # calculates zero-based ranks
        return 1 - (2.0 * (r * x).sum() + s) / (n * s)
    def getShares(x, y):
        """
        gets shares in Y, based on percentiles over x
        """
        if False:
            percentiles = np.arange(20, 120, 20)
            xx = x.reshape((-1)).copy()
            xx.sort()
            xxPercentiles = [np.percentile(xx, p) for p in percentiles]
            indices = [np.argmin(np.abs(p - xx)) for p in xxPercentiles]
            return np.array([xx[index].cumsum()[0] for index in indices]) / xx[-1].cumsum()
        xxPercentiles = getPercentiles(x)
        shares = np.zeros(len(xxPercentiles))
        for i in np.arange(len(xxPercentiles)):
            if i == 0:
                lower = 0
            else:
                lower = xxPercentiles[i - 1]
            shares[i] = y[(x > lower) & (x <= xxPercentiles[i])].sum()/y.sum()
        return shares

    def getPercentiles(x):
        percentiles = np.arange(20, 120, 20)
        xx = x.reshape((-1)).copy()
        xx.sort()
        xxPercentiles = [np.percentile(xx, p) for p in percentiles]
        return xxPercentiles

    # variances of log earnings and consumption by age
    vS = np.log(Y).std(axis=0)
    vC = np.log(X -  S).std(axis=0)
    # average earnings, consumption and wealth by age
    mY = Y.mean(axis=0)
    mC = (X - S).mean(axis=0)
    mX = S.mean(axis=0)
    # gini coefficients for earnings and wealth
    gY = getGini(Y.reshape((-1)))
    gX = getGini(S.reshape((-1)))
    # shares of income, consumption and wealth by quintiles of wealth distribution
    incomeShares = getShares(S, Y)
    consumptionShares = getShares(S, X-S)
    wealthShares = getShares(S, S)

    return {'vS': vS, 'vC':vC, 'mY':mY, 'mC':mC, 'mX':mX, 'gY':gY, 'gX':gX,
            'incomeShares':incomeShares, 'consumptionShares':consumptionShares, 'wealthShares':wealthShares}

def getKandW(r, Param):
    """
    solves K, w, for given interest rate, from firms' problem
    :param r: interest rate
    :param Param: parameters
    :return: Tuple: (KDemand, w)
    """
    # with deterministic death, we have each generation equally represented:
    lBar = Param.G.cumprod().mean()
    #g = Param.G.copy()
    #g[Param.TRet:] = 1
    #lBar = g.cumprod().mean()
    KDemand = (Param.alpha / (r + Param.delta)) ** (1 / (1 - Param.alpha)) * lBar
    #w = (1-Param.alpha) * (Param.alpha/(r + Param.delta))**(1./(1-Param.alpha))
    w = (r + Param.delta) * (1-Param.alpha)/Param.alpha * KDemand/lBar
    #w = (1-Param.alpha) * KDemand**Param.alpha * lBar**(-Param.alpha)
    # enforce smoothness for weird r
    if w > 100:
        print 'Warning: w hit upper bound: ' + str(w)
        w = 100

    return KDemand, w

def solveCapitalSupply(r, w, method, simulateMethod, Param):
    """
    solves for a(r), capital supply
    :param r: interest rate
    :param w: wages
    :param theta: death rates
    :param WFunction: bequest function
    :param method: method of solving the problem (solveValueFunction, endogenousGridPointMethod)
    :param Param: parameters
    :return: total capital
    """
    policies = method(r=r, w=w, Param=Param)
    S, X, Y = simulateMethod(policies=policies, Param=Param, r=r, w=w)
    # i assume measure 1 of households
    totalSavings = S.mean()
    return totalSavings

def solveEquilibrium(method, simulateMethod, Param):
    """
    solves for r: a(r) == k(r). Stores also in Param.r
    :param theta: death rates
    :param WFunction: bequest function
    :param method: method of solving the problem (solveValueFunction, endogenousGridPointMethod)
    :param Param: parameters
    :return: r equilibrium interest rate
    """
    def solveEquilibriumError(r, method, simulateMethod, Param):
        KDemand, w = getKandW(r, Param)
        capitalSupply = solveCapitalSupply(r, w, method, simulateMethod, Param)
        return capitalSupply -KDemand
    r, results = optimize.brentq(solveEquilibriumError, -Param.delta + 1e-6, 3, args=(
        method, simulateMethod, Param), full_output=True, xtol=1e-6)
    # solveEquilibriumError(2, theta, WFunction, method, Param)
    KDemand, w = getKandW(r, Param)
    capitalSupply = solveCapitalSupply(r, w, method, simulateMethod, Param)
    #print np.abs(KDemand/capitalSupply - 1)
    if np.abs(KDemand/capitalSupply - 1) > 0.1:
        print 'Warning: Did not find a good solution for r given beta = ' + str(Param.beta) + \
              ' (error of ' + str(np.abs(KDemand/capitalSupply - 1)) + ' )'

    Param.r = r
    return r

def calibrateCapitalOutputRatioInefficient(target, method, simulateMethod, Param):
    """
    Solves for beta : K(beta)/Y(beta) == target
    Note: Changes Param.beta
    :param theta: death rates
    :param WFunction: bequest function
    :param method: method of solving the problem (solveValueFunction, endogenousGridPointMethod)
    :param Param: parameters
    :return: optimal beta
    """
    def calibrateCapitalOutputRatioError(beta, target, method, simulateMethod, Param):
        Param.beta = beta
        r = solveEquilibrium(method, simulateMethod, Param)
        Param.r = r
        KDemand, w = getKandW(r, Param)
        lBar = Param.G.cumprod().mean()

        ratio = (KDemand/lBar)**(1-Param.alpha)
        return ratio-target


    beta, output  = optimize.brentq(calibrateCapitalOutputRatioError, 0.7, 1.3,
                               args=(target, method, simulateMethod, Param),
                               full_output=True)
    #calibrateCapitalOutputRatioError(Param.beta, target, theta, WFunction, method, Param)
    if beta >= 1:
        raise RuntimeWarning('calibrated beta >= 1: ' + str(beta))
    Param.beta = beta
    return beta

def calibrateCapitalOutputRatio(target, method, simulateMethod, Param):
    """
    Solves for beta : K(beta)/Y(beta) == target
    Note: Changes Param.beta
    :param theta: death rates
    :param WFunction: bequest function
    :param method: method of solving the problem (solveValueFunction, endogenousGridPointMethod)
    :param Param: parameters
    :return: optimal beta
    """

    labor = Param.G.cumprod().mean()

    r = Param.alpha/target - Param.delta
    #K = target**(1/(1-Param.alpha)) * labor
    K, w = getKandW(r, Param)

    def calibrateCapitalOutputRatioError(beta, target, method, simulateMethod, Param):
        Param.beta = beta
        policies = method(r=r, w=w, Param=Param)
        S, X, Y = simulateMethod(r=r, w=w, policies=policies, Param=Param)
        return S.mean() - K

    beta, output = optimize.brentq(calibrateCapitalOutputRatioError, 0.7, 1.3,
                                   args=(target, method, simulateMethod, Param),
                                   full_output=True)
    # calibrateCapitalOutputRatioError(Param.beta, target, theta, WFunction, method, Param)
    if beta >= 1:
        raise RuntimeWarning('calibrated beta >= 1: ' + str(beta))
    Param.beta = beta
    return beta



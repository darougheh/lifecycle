import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
np.seterr(all='warn')
from scipy import optimize

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
np.random.seed(1)
import scipy.interpolate as interpolate
interp1d = interpolate.interp1d
from scipy.interpolate import InterpolatedUnivariateSpline
import numpy.random as random
def extrap1d(interpolator):
    """
    combines interpolate and extrapolate, as in Matlab
    :param interpolator: scipy interpolate object
    :return:
    """
    xs = interpolator.x
    ys = interpolator.y

    def pointwise(x):
        if x < xs[0]:
            return ys[0]+(x-xs[0])*(ys[1]-ys[0])/(xs[1]-xs[0])
        elif x > xs[-1]:
            return ys[-1]+(x-xs[-1])*(ys[-1]-ys[-2])/(xs[-1]-xs[-2])
        else:
            return interpolator(x)

    def ufunclike(xs):
        return np.array(map(pointwise, np.array(xs)))

    return ufunclike

class Parameters(object):
    '''
    contains the parameters, and creates the grid points
    '''
    def __init__(self):

        self.T = np.float64(65)
        self.I = 10000 # number of individuals for simulation
        self.TRet = np.float64(45) #period of retirement

        self.sigma = np.float64(2) # risk averison
        self.rho = np.float64(0.04) # discount rate

        self.r = np.float64(0.02) # interest rate

        self.varN = np.float64(0.0106) # variance of permanent shocks
        self.varV = np.float64(0.0738) # -- transitory shocks
        self.nY = np.float64(5) # number of nodes in quadrature of income
        self.probV0 = np.float64(0.0005) # probability of zero income: 0 => explicit
        #% life        cycle        profile        of        income(deterministic)
        self.G = np.array([15.5273036489961,1.13087311215982,1.05870874450936,1.05467219182954,1.05077711505543,
             1.04702205854521,1.04340562226528,1.03992646092071,1.03658328312142,1.03337485058359,
             1.03029997736546,1.02735752913683,1.02454642248183,1.02186562423414,1.01931415084419,
             1.01689106777780,1.01459548894578,1.01242657616384,1.01038353864259,1.00846563250697,
             1.00667216034486,1.00500247078444,1.00345595809984,1.00203206184495,1.00073026651485,
             0.999550101234814,0.998491139476392,0.997552998800479,0.996735340627130,0.996037870031881,
             0.995460335568447,0.995002529117637,0.994664285762371,0.994445483688613,0.994346044112305,
             0.994365931232058,0.994505152207672,0.994763757164433,0.995141839223198,0.995639534556274,
             0.996257022469168,0.996994525508269,0.997852309594495,0.998830684183142,0.999930002449943,
             0.682120000000000,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1], dtype=np.float64)


        self.sMin = 0
        self.sMax = 20
        self.spacing = 1.5
        self.nS = 250
        self.nS = 15
        self.nX = 200
        self.nX = 10
        self.xMin = 0.001
        self.xMax = 30


        self.beta = 1 / (1 + self.rho)
        self.setGrids()

    def setGrids(self):
        # savings
        self.sGrid = np.zeros((self.nS,))
        self.sGrid[0] = self.sMin
        for i in range(1, self.nS):
            self.sGrid[i] = self.sGrid[i-1] + (self.sMax - self.sGrid[i-1])/((self.nS - i)**self.spacing)

        # cash on hand
        self.xGrid = np.zeros((self.nX))
        self.xGrid[0] = self.xMin
        for i in range(1, self.nX):
            self.xGrid[i] = self.xGrid[i - 1] + (self.xMax - self.xGrid[i-1]) / ((self.nX - i) ** self.spacing)


        self.muN = -self.varN*0.5
        self.muV = np.log(float(1)/(1 - self.probV0)) - self.varV * 0.5

        import numpy.polynomial.hermite as hermite
        nodes, weights = hermite.hermgauss(self.nY)
        self.nGrid = np.exp(np.sqrt(2 * self.varN) * nodes + self.muN)
        self.vGrid = np.exp(np.sqrt(2 * self.varV) * nodes + self.muV)
        self.pN = weights / np.pi ** 0.5
        self.pV = weights / np.pi ** 0.5
        self.nN, self.nV = self.nY, self.nY
        if self.probV0 > 0:
            self.nV += 1
            self.vGrid = np.hstack((0, self.vGrid))
            self.pV = np.hstack([self.probV0, (1-self.probV0) * self.pV])

        #% construct         long         vectors        for both shocks: all        possible        combinations and
        #% corresponding         weights(used in policy        function        iteration        to        avoid
        #the        % loops        over        nodes)

        self.PV, self.PN = np.meshgrid(self.pV, self.pN, indexing='ij')
        self.V, self.N = np.meshgrid(self.vGrid, self.nGrid, indexing='ij')
        self.X, self.S, = np.meshgrid(self.xGrid, self.sGrid, indexing='ij')

def Utility(c, Param):
    """
    :param c: Consumption
    :param Param: Parameters object
    :return: u(c)
    """
    if Param.sigma == 1:
        return np.log(c)
    else:
        return c**(1-Param.sigma)/(1-Param.sigma)

def fPrime(c, Param):
    """
    :param c: consumption
    :param Param:  Parameters object
    :return: u'(c)
    """
    return c**(-Param.sigma)

def cOfFPrime(f, Param):
    """
    :param f: uTilde
    :param Param: Parameters object
    :return: c: u'(c) = uTilde
    """
    return f**(-1/Param.sigma)

def solveValueFunction(Param):
    """
    solves the value function
    :param Param: Parameters
    :return: S, V (optimal savings, value function)
    """
    X, S = Param.X, Param.S
    C = X - S
    iValid = C > 0
    cValid = iValid * C + (1-iValid)*1

    uMinimum = Utility(np.min(Param.xGrid), Param)

    U = iValid * Utility(cValid, Param) + (1-iValid)*(uMinimum - 1000)

    WResult = np.zeros((Param.nX, int(Param.T))) # value function
    SResult = np.zeros((Param.nX, int(Param.T))) # savings policy

    WResult[:,-1] = Utility(Param.xGrid, Param)
    SResult[:,-1] = 0

    for t in np.arange(0, Param.T-1, dtype=int)[::-1]:
        print t,
        interpolateInter = interp1d(Param.xGrid, WResult[:, t + 1], kind='linear')
        interpolateExtra = extrap1d(interpolateInter)
        from scipy.interpolate import InterpolatedUnivariateSpline
        interpolateMe = InterpolatedUnivariateSpline(Param.xGrid, WResult[:, t + 1],
                                                     k=1)

        if t+1 < Param.TRet:
            # compute expected return by iterating through shocks, but having a S grid
            wPrime = np.zeros((Param.nS))
            for i in np.arange(0, Param.nN):
                for j in np.arange(0, Param.nV):
                    xPrime = Param.sGrid * (1+Param.r)/Param.G[t+1] * Param.nGrid[i] + Param.vGrid[j]
                    #wTemp = interpolateMe(xPrime)
                    wTemp = interpolateMe(xPrime.reshape(-1)).reshape(xPrime.shape)
                    #wTemp = scipy.interpolate()
                    wPrime += Param.pN[i]*Param.pV[j]*Param.G[t+1]*Param.nGrid[i]**(1-Param.sigma) * wTemp
        else: # no uncertainty
            xPrime = Param.sGrid * (1+Param.r)/Param.G[t+1] + 1
            wPrime = Param.G[t+1]**(1-Param.sigma) * interpolateMe(xPrime)

        # @todo might need to adjust wPrime
        wMatrix = U + Param.beta* wPrime
        SResult[:, t] = Param.sGrid[wMatrix.argmax(axis=-1)]
        WResult[:, t] = wMatrix.max(axis=-1)

    print ' done'
    return SResult, WResult

def solvePolicyFunction(Param):
    """
    Solves the corresponding policy function
    :param Param: Parameters object
    :return: C (optimal consumption)
    """
    def policyResidual(logC, x, cNext, t, Param):
        c = np.exp(logC)
        if c> x:
            return 1e20
        from scipy.interpolate import InterpolatedUnivariateSpline
        interpolateMe = InterpolatedUnivariateSpline(
            np.hstack([0, Param.xGrid]),
            np.hstack([0, cNext]),
            k=1)

        if t+1 < Param.TRet:
            xPrime = (x-c)*(1+Param.r)/(Param.G[t+1] * Param.N) + Param.V
            cPrime = interpolateMe(xPrime.reshape((-1))).reshape(xPrime.shape)
            muPrime = ( Param.PN*Param.PV * (Param.G[t+1]*Param.N)**(-Param.sigma)
                        * fPrime(cPrime, Param) ).sum()
        else:
            xPrime = (x - c) * (1 + Param.r) / Param.G[t + 1] + 1
            cPrime = interpolateMe(xPrime)
            muPrime = Param.G[t+1]**(-Param.sigma) * fPrime(cPrime, Param)
        RHS = Param.beta*(1+Param.r)*muPrime
        LHS = fPrime(c, Param)
        return np.abs(RHS - LHS)


    cResult = np.zeros((Param.nX, Param.T))
    cResult[:,Param.T-1] = Param.xGrid
    import scipy.optimize as optimize
    for t in np.arange(0, Param.T-1, dtype=int)[::-1]:
        print t,
        for i in np.arange(0, Param.nX):
            logCInit = np.log((1-Param.beta)*Param.xGrid[i])

            logC = optimize.fmin(policyResidual, logCInit, args=(
                Param.xGrid[i], cResult[:, t+1], t, Param),
                disp=False)
            cResult[i, t] = np.exp(logC)

    print 'done'
    return cResult

def solveEndogenousGridPoints(Param):
    """
    solves through endogenous grid points method
    :param Param: Parameters object
    :return: (C, X) - consumption, (T,) shape of grid points
    """
    sGrid = Param.sGrid.copy()

    # if p0 > 0, zero savings are never optimal. Endogenous grid points only works for s which is sometimes optimal
    if Param.probV0 > 0:
        sGrid = sGrid[1:]
    nS = len(sGrid)
    cResult = np.zeros((nS, Param.T))
    xResult = np.zeros((nS, Param.T))

    xResult[:, -1] = np.linspace(0.01, 10, nS) # arbitrary grid
    cResult[:, -1] = xResult[:, -1]

    for t in np.arange(0, Param.T-1, dtype=int)[::-1]:
        print t,
        from scipy.interpolate import InterpolatedUnivariateSpline
        interpolateMe = InterpolatedUnivariateSpline(np.hstack((0,xResult[:,t+1])),
                                                     np.hstack((0,cResult[:, t + 1])),
                                                     k=1)

        if t+1 < Param.TRet:
            gothicV = np.zeros((nS))
            for i in np.arange(0, Param.nN):
                for j in np.arange(0, Param.nV):
                    xPrime = sGrid * (1+Param.r)/(Param.G[t+1]*Param.nGrid[i]) + Param.vGrid[j]
                    cPrime = interpolateMe(xPrime)
                    gothicV += ( Param.beta * (1+Param.r) * Param.pN[i] * Param.pV[j] *
                        ( Param.G[t+1] * Param.nGrid[i])**(-Param.sigma) * fPrime(cPrime, Param))
        else:
            xPrime = sGrid * (1+Param.r)/(Param.G[t+1]) + 1
            cPrime = interpolateMe(xPrime)
            gothicV = Param.beta * (1+Param.r) * (Param.G[t+1])**(-Param.sigma) * fPrime(cPrime, Param)

        cResult[:,t] = cOfFPrime(gothicV, Param)
        xResult[:,t] = cResult[:,t] + sGrid
    print ' done'
    return cResult, xResult

def solve(method, Param):
    """
    abstraction for the three solution methods
    :param method:
    :param Param:
    :return:
    """
    result = method(Param)

    if method == solveEndogenousGridPoints:
        c, x = result
        s = x - c
        Param.xGrid = x
    elif method == solvePolicyFunction:
        c = result
        s = Param.xGrid[..., np.newaxis] - c
    elif method == solveValueFunction:
        s, w = result
        c = Param.xGrid[..., np.newaxis] - s
    else:
        raise BaseException('invalid method')
    return c, s

def simulate(policies, method, Param):
    """
    simulates the economy
    :param policies: optimal policy
    :param method:
    :param Param:
    :return:
    """

    c,s = policies
    # permanent
    N = np.ones((Param.I, Param.T))
    N[:,0:Param.TRet] = random.lognormal(Param.muN, np.sqrt(Param.varN), (Param.I, Param.TRet ) )

    # transitory
    V = np.ones((Param.I, Param.T))
    V[:, 0:Param.TRet] = random.lognormal(Param.muV, np.sqrt(Param.varV), (Param.I, Param.TRet))
    if Param.probV0 > 0:
        incZero = random.uniform(0, 1, size=(Param.TRet))
        V[:, :Param.TRet] *= (incZero > Param.probV0).astype(float)

    # permanent income component
    P = np.ones((Param.I, Param.T))
    P[:,0] = Param.G[0] * N[:,0]
    for t in np.arange(1,Param.T):
        P[:,t] = Param.G[t] * P[:,t-1] * N[:,t]

    Y = P*V

    normX = np.zeros((Param.I, Param.T) ) # evolution of states: normalized cash on hands
    normS = np.zeros((Param.I, Param.T)) # simulated behavior: normalized savings

    normX[:,0] = V[:,0]

    for t in np.arange(0,Param.T-1):

        if method == solveEndogenousGridPoints:
            interpolateMe = InterpolatedUnivariateSpline(
                np.hstack((0,Param.xGrid[:, t])),
                np.hstack((0, s[:, t])),
                k=1)
        else:
            interpolateMe = InterpolatedUnivariateSpline(
                np.hstack((0, Param.xGrid)),
                np.hstack((0, s[:, t])),
                k=1)

        normS[:,t] = interpolateMe(normX[:,t].reshape(-1)).reshape(normX[:,t].shape)
        normX[:,t+1] = (1+Param.r) * normS[:,t] * (Param.G[t+1] * N[:,t+1])**(-1) + V[:,t+1]

    S = normS * P
    X = normX * P

    return S, X, Y

Param = Parameters()

c, s = solve(solveEndogenousGridPoints, Param)



if True:
    method = solveEndogenousGridPoints
    policies = solve(method, Param)
    S, X, Y = simulate(policies, method, Param)

    plt.plot(S.mean(axis=0), label='median S')
    plt.plot((X-S).mean(axis=0), label='median C')
    plt.plot(Y.mean(axis=0), label='median Y')
    plt.legend()
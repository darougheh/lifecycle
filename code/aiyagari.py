"""
Solves aiyagari economy, where L is AR-1 shock, household income is wL + rK
"""

import numpy as np
import scipy.optimize as optimize
from scipy.interpolate import InterpolatedUnivariateSpline
import marcov


class Parameters(object):

    def __init__(self):

        # parameters
        self.sigma = 1.5
        self.alpha = 0.33
        self.delta = 0.1

        self.rho = 0.9
        self.stdL = 0.5

        # asset grid
        self.aMax = 10
        self.aMin = 0
        self.nA = 10

        # shock Grids
        self.lMax = 5
        self.lMin = 1
        self.nL = 5

    def createGrids(self, w):
        self.aGrid = np.logspace(np.log10(self.aMin), np.log10(self.aMax), self.nA)
        #self.lGrid = np.logspace(np.log10(self.lMin), np.log10(self.lMax), self.nL)
        self.lGrid, self.transitionL = marcov.getMarcovChain(self.rho, self.stdL, 2, self.nL )
        self.A, self.L, self.APrime = np.meshgrid(self.aGrid, self.lGrid, self.aGrid, indexing='ij')


def utility(c, Param):
    return c**(1-Param.sigma)/(1-Param.sigma)

def solveBellmanEquation(r, w, Param):
    """
    solves household problem given r, k
    returns V, policy functions
    household problem: max aPrime, c: u(c) + \beta E*vaPrmie, cPrime)s.t. c + aPrime = (1+r)a + wy, aPrime geq 0
    firm problem: max F - (r+delta)K - wL
    """
    Param.createGrids(w)
    A, L, APrime = Param.A, Param.L, Param.APrime
    C = (1+r)*A + w*L - APrime
    cValid = C > 0
    C[cValid == False] = 1
    U = utility(C, Param)
    U[cValid == False] = U.min() + 1000*C[cValid == False]
    vOld = np.ones(A.shape) * 10
    vOldMax = np.ones(A.max(axis=-1).shape)
    vOldArgMaxA = np.ones(A.argmax(axis=-1).shape)

    for i in range(1000):
        interpolateMe = InterpolatedUnivariateSpline(Param.aGrid, vOldArgMaxA, k=1)
        # vOldMax has shape a, l
        VTemp = np.zeros(Param.lGrid.shape)
        for j in range(Param.nL):
            xPrime = APrime*(1+r) + Param.lGrid[i]*w
            VPrime = interpolateMe(xPrime.reshape((-1))).reshape(xPrime.shape)
            VTemp += VPrime*

            # @todo
        V = U + Param.beta * VTemp
        VMax = V.max(axis=-1)
        VArgMaxA = V.argmax(axis=-1)

        if (np.abs(VMax - VOldMax).max() < 1e-3):
            return VMax, VArgMaxA
        VOldMax = VMax
        VOldArgMaxA = VArgMaxA
    else:
        raise Exception('did not converge. Maximum difference: ' + str(np.abs(VMax - VOldMax).max()))
    return V, Param.aGrid[argMaxA]

def solveForEquilibrium(Param):
    def errorEquilibrium(r, Param):

        # first: solve for w
        #lDemand = ((1 - Param.alpha) / (w)) ** (1. / Param.alpha)
        # F_L = w = (1-alpha)L^(-alpha)K^alpha
        # w =
        kDemand = (Param.alpha / (r + Param.delta)) ** (1. / (1 - Param.alpha))
        w = (1-Param.alpha)*1. * kDemand**(Param.alpha)

        V, policies = solveBellmanEquation(r, w, Param)
        savings = policies
        distributionOverX = np.ones(Param.xGrid) * 1. / (len(Param.xGrid))
        kSupply = (distributionOverX * savings).sum()

        measure = (kSupply-kDemand)**2
        return measure

    # due to uniqueness and global convergence, we can bisect
    # 5 should be large enough
    rOptimal = optimize.bisect(errorEquilibrium, -Param.delta+1e-6, 5, args=(Param,))
    return rOptimal

Param = Parameters()
solveForEquilibrium(Param)
from scipy.stats import norm
from numpy import *



def getMarcovChain (rho, sigma, N, m):
    """create transition matrix and and state space for y_t = rho*y_t-1 + u_t"""
    lambdda = rho
    " , where ut distributed by a gaussian with std sigma"
    " , N is the number of states"
    " , m = Ymax/Vary = - Ymin/Vary"

    sdY = (sigma**2/(1-lambbda**2))**(1/2)
    yMax = m*sdY
    yMin = -yMax
    w = (yMax-yMin)/(N-1) # length of each discretized state
    s = linspace(yMin, yMax, N)
    Tran = zeros((N,N))
    for j in arange(0, N):
        for k in arange(1, N-1):
            
            C1 = norm.cdf(s[k]- lambbda*s[j] + w/2, scale=sigma)
            C2 = norm.cdf(s[k]- lambbda*s[j] - w/2, scale=sigma)


            Tran[j,k] = C1-C2
            #import pdb; pdb.set_trace()
        Tran[j,0] = norm.cdf(s[0]- lambbda*s[j] + w/2, scale=sigma)
        Tran[j,N-1] = 1 - norm.cdf(s[N-1]- lambbda*s[j] - w/2, scale=sigma)   
    

    if any(abs(Tran.sum(axis=1) -1) > exp(-10)):        
        print Tran.sum(axis=1)
        print Tran
        raise Exception ("some axis=1 does not sum up")
    return [s, Tran]

